﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Splicer : ENT_Interact
{


    public ENT_DungeonEntity.monsterTypes ingredient0;
    public ENT_DungeonEntity.monsterTypes ingredient1;
    public string path_to_result_prefab;

    private GameObject minion0 = null;
    private GameObject minion1 = null;

    private PLY_AllyManager allyManager;


    // Use this for initialization
    public override void Start()
    {
        base.Start();
        allyManager = player.GetComponent<PLY_AllyManager>();
    }

    public override void activate()
    {
        base.activate();
        foreach (List<GameObject> group in allyManager.getAllies())
        {
            foreach (GameObject ally in group)
            {
                ENT_Body myBody = ally.GetComponent<ENT_Body>();
                if (myBody.amINearTwig())
                {
                    //Debug.Log("Judging Minions Based on Thier Body Types");
                    if (myBody.getMyType() == ingredient0)
                    {
                        minion0 = ally;
                    }
                    else if (myBody.getMyType() == ingredient1)
                    {
                        minion1 = ally;
                    }
                    if (minion0 != null && minion1 != null)
                    {
                        //Debug.Log("Found Match");
                        allyManager.removeAlly(minion0);
                        Destroy(minion0);
                        allyManager.removeAlly(minion1);
                        Destroy(minion1);
                        //print("location:" + (gameObject.transform.position - player.transform.position) / 2);
                        UTL_GameObject.cloneAtLocation(path_to_result_prefab, gameObject.transform.position + (gameObject.transform.position - player.transform.position) / 2);
                        //print("created new minion");
                        minion0 = null;
                        minion1 = null;
                        break;
                    }
                }
            }
        }
    }


}
