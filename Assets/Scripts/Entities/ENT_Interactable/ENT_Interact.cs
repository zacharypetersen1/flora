﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Interact : MonoBehaviour {

    //player is near interable 
    //Change from static to something else later
    public static GameObject triggerable;
    //[HideInInspector]
    public FUI_Interactable fui;
    public GameObject player;
    public bool destroy_after_use;
    public bool snapToTerrainPos = true;
    //
    // Use this for initialization
    //
    public virtual void Start()
    {
        fui = gameObject.GetComponent<FUI_Interactable>();
        player = GameObject.Find("Player");
        if (snapToTerrainPos) transform.position = UTL_Dungeon.getTerrPosition(transform.position);
        //print("fui " + fui);
    }

    // Update is called once per frame
    public virtual void Update () {
        if (triggerable == gameObject)
        {
            if (INP_PlayerInput.getButtonDown("Interact"))
            {
                activate();
            }
        }
	}


    public virtual void setTriggerable(GameObject obj)
    {
        triggerable = obj;
    }

    //
    // Show interactable button prompt and make interactable triggerable
    //
    public virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            setTriggerable(gameObject);
            fui.show();
        }
        //can_interact = true;
    }



    //
    // Hide interactable button prompt and make interactable untriggerable
    //
    public virtual void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            setTriggerable(null);
            fui.hide();
        }
        //can_interact = false;
    }


    public virtual void activate()
    {
        if (destroy_after_use)
        {
            setTriggerable(null);
            fui.hide();
            Destroy(gameObject);
        }
    }
}
