﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_PlayerBody : ENT_DungeonEntity
{



    public UIX_CameraFacingBillboard_Minion healthBar;

    /*
    [FMODUnity.EventRef]
    public string TwigHit;
    */

    //
    //
    //
    public override void activate()
    {

    }



    //
    // Use this for initialization
    //
    public override void Start ()
    {
        CL = CL_MAX;
        //healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();
    }



    //
    // Update is called once per frame
    //
    public override void Update ()
    {
		
	}



    //
    // Runs when player takes damage
    //
    public override void inflict_damage(float DC)
    {
        CL -= DC;
    }



    //
    // Runs any time the players CL changes
    //
    public override void on_delta_clarity()
    {
        healthBar.updateHealthBar(CL_MAX, CL);
    }
}
