﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_NecroMinion : ENT_Body {

    public float resurrectDistance = 30f;

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public void resurrect()
    {
        List<GameObject> remnants = UTL_Dungeon.getNearestListWithTag(transform.position, "remnant", resurrectDistance);
        if (remnants == null) return;
        foreach(GameObject remnant in remnants)
        {
            GameObject ghost = remnant.GetComponent<ENT_Remnant>().revive(Purity);
            Destroy(remnant);

        }
    }
}
