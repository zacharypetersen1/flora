﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ConfusionAbility : ENT_MinionAbility
{

    private GameObject confusionAbility;

    protected override void _allyAbility()
    {
        base._allyAbility();
        print("allyAbility entered");
        confusionAbility = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/ConfusionAbility/confusionAbility", gameObject.transform.position);
        confusionAbility.transform.SetParent(gameObject.transform);
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purtiy = true;
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        confusionAbility = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/ConfusionAbility/confusionAbility", gameObject.transform.position);
        confusionAbility.transform.SetParent(gameObject.transform);
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purtiy = false;
    }

    public override void endAllyAbility()
    {
        print("endAllyAbility entered");
        base.endAllyAbility();
        //Debug.Log(gameObject.GetComponent<ENT_Body>().IsAbilityActive);
        //Destroy(slowAbility);
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        //Destroy(slowAbility);
    }
}
