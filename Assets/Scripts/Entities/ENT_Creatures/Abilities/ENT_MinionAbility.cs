﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//base class for minion abilities. Extend this for each minion ability
public class ENT_MinionAbility : MonoBehaviour {

    public float enemyMaxCoolDown = 10;
    public float allyMaxCoolDown = 10;

    private float enemyCoolDownTimer;
    private float allyCoolDownTimer;

    private ENT_Body myBody;
    // Use this for initialization
    virtual public void Start () {
        enemyCoolDownTimer = enemyMaxCoolDown;
        allyCoolDownTimer = allyMaxCoolDown;
        myBody = gameObject.GetComponent<ENT_Body>();

    }
	
	// Update is called once per frame
	virtual public void Update () {
        enemyCoolDownTimer = Mathf.Max(0F, enemyCoolDownTimer - TME_Manager.getDeltaTime(TME_Time.type.enemies));
        allyCoolDownTimer = Mathf.Max(0F, allyCoolDownTimer - TME_Manager.getDeltaTime(TME_Time.type.enemies));
    }

    public void resetTimers()
    {
        enemyCoolDownTimer = enemyMaxCoolDown;
        allyCoolDownTimer = allyMaxCoolDown;
    }

    public bool activateEnemyAbility()
    {
        if (myBody.Purity) {
            print("cannot activate enemy ability from an ally minion");
            return false;
        }
        if (enemyCoolDownTimer > 0)
        {
            return false;
        }
        else
        {
            _enemyAbility();
            resetTimers();
            return true;
        }
    }

    public bool activateAllyAbility()
    {
        if (!myBody.Purity)
        {
            print("cannot activate ally ability from an enemy minion");
            return false;
        }
        if (allyCoolDownTimer > 0)
        {
            print(gameObject.name + "'s ability still on cooldown");
            return false;
        }else
        {
            _allyAbility();
            resetTimers();
            return true;
        }  
    }

    //override these
    virtual protected void _enemyAbility()
    {
        myBody.IsAbilityActive = true;
    }

    //override these
    virtual protected void _allyAbility()
    {
        myBody.IsAbilityActive = true;
    }

    virtual public void endEnemyAbility()
    {
        myBody.IsAbilityActive = false;
        
    }

    virtual public void endAllyAbility()
    {
        myBody.IsAbilityActive = false;
    }
}
