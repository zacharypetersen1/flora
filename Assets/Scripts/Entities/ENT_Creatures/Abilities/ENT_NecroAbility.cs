﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_NecroAbility : ENT_MinionAbility
{
    protected override void _allyAbility()
    {
        base._allyAbility();
        gameObject.GetComponent<ENT_NecroMinion>().resurrect();
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
    }

    public override void endAllyAbility()
    {
        base.endAllyAbility();
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
    }
}
