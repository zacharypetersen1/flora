﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class ENT_Body : ENT_DungeonEntity
{
    public bool startsAsAlly = false;
    public bool purifiable = true;
    public Material evilMaterial;
    public Material goodMaterial;
    public float attackRechargeTime = 1f;
    public monsterTypes entity_type;
    [HideInInspector]
    public bool vulnerable = true;

    //-- start behavior trees
    public BrickAsset AttackTree;
    public BrickAsset WorkTree;
    public BrickAsset LookaroundTree;

    public BrickAsset ConfusionTree;

    public BrickAsset AllyFollowTree;
    public BrickAsset AllyWaitTree;
    public BrickAsset AllyGoToTree;
    public BrickAsset AllyActivateTree;
    public BrickAsset AllyAttackTree;
    //-- end behavior tree variables

    public float threatRange = 10; //the range at which this monster will choose to attack it's enemies
    public float attackRange = 5; //the range at which a monster must be to actually initiate an attack
    public float sightRange = 30; //how far away the monster can see objects
    public float sightRadius = 90F; //the LOS radius 
    public float pickupRange = 3; //the range at which this minion can pick up crystals

    //public float nearFollowRadius;
    public float farFollowRadius;

    public float normalAttackSpeed = 1f;
    public float slowAttackSpeed = 1f;

    public int normalSpeed = 15;
    public int surfingSpeed = 20;
    public int slowSpeed = 7;

    protected float patrolTimer = 0F;

    public GameObject spawnedBy;
    public GameObject partolTarget;
    public GameObject workTarget;

    public bool finishedSearchLeft = true;
    public bool finishedSearchForward = true;
    public Vector3 searchForward;
    public Vector3 searchLeft;
    public Vector3 searchRight;
    public bool startSearch = true;
    public float searchTimer = 0F;
    const float SEARCH_TIME = 10F;

    public AIC_AllyGroup commandGroup;

    public bool shouldRotate = false;
    public Vector3 lookTowardsVec; //also set shouldRotate to true if you change this.

    public bool carrying = false;
    public Vector3 enemyWaitPosition = new Vector3(0, 0, 0);

    private PLY_PlayerDriver playerDriver;

    //public UIX_CameraFacingBillboard_Minion healthBar;

    protected const int NUMWANDERPOINTS = 4;

    //private Vector3 patrolPos;
    private int patrolIndex = 0;
    public bool startPatrol = false; //should be set to true on entering patrol state

    public bool isAbilityActive = false;
    public bool IsAbilityActive
    {
        get { return isAbilityActive; }
        set { isAbilityActive = value;
            //print("setting isAbilityActive to " + value); 
        }
    }

    public float positionSensitivity;

    public int confusedIndex = 0;
    public bool startConfusion = false;

    public List<Pair<CBI_CombatMessenger.status, float>> StatusList = new List<Pair<CBI_CombatMessenger.status, float>>();

    private ENT_MinionAbility ability;

    private List<Pair<CBI_CombatMessenger.status, float>> removeList = new List<Pair<CBI_CombatMessenger.status, float>>();

    private List<Vector3> _PatrolPoints = new List<Vector3>();
    public List<Vector3> PatrolPoints
    {
        get { return _PatrolPoints; }
        set
        {
            _PatrolPoints.Clear();
            if (value == null || value.Count == 0)
            {
               _PatrolPoints = setRandomPatrolPoints();
                return;
            }
            foreach (Vector3 vec in value)
            {
                Vector3 new_vec = UTL_Dungeon.getTerrPosition(vec);
                _PatrolPoints.Add(new_vec);
            }
        }
    }

    private List<Vector3> _ConfusedPoints = new List<Vector3>();
    public List<Vector3> ConfusedPoints
    {
        get { return _ConfusedPoints; }
        set
        {
            _ConfusedPoints.Clear();
            if (value == null || value.Count == 0)
            {
                _ConfusedPoints = setRandomPatrolPoints();
                return;
            }
            foreach (Vector3 vec in value)
            {
                Vector3 new_vec = UTL_Dungeon.getTerrPosition(vec);
                _ConfusedPoints.Add(new_vec);
            }
        }
    }

    public enum BehaviorTree
    {
        AttackTree, WorkTree, lookaroundTree, AllyFollowTree, AllyWaitTree, AllyGoToTree, AllyActivateTree, AllyAttackTree, ConfusionTree
    }

    //protected CHA_Motor motor;
    protected NavMeshAgent agent;
    public NavMeshAgent Agent
    {
        get { return agent; }
    }
    protected PTH_Path path;
    protected BehaviorExecutor AI;

    public Vector3 endPoint{
        get { return agent.destination; }
        set { goToPosition(value); }
    }

    Vector3 pastPos;
    bool move = true;
    float attackCooldown; // 0 when attack is ready, gets set to attackRechargeTime on attack, goes down by 1/second

    private bool generatePathThisFrame = true; //whether or not the minion should generate a new path each frame

    protected GameObject _crystal;
    public GameObject crystal
    {
        get { return _crystal; }
        set { setTargetCrystal(value); }
    }

    protected Vector3 waitPos;
    public Vector3 WaitPos
    {
        get { return waitPos; }
        set { waitPos = value; }
    }

    protected GameObject _attackTarget;
    public GameObject AttackTarget
    {
        get { return _attackTarget; }
        set { _attackTarget = value; }
    }
    //private GameObject nearestCrystal;
    BehaviorTree lastTree;
    protected BehaviorTree _currentTree;
    public BehaviorTree CurrentTree
    {
        get { return _currentTree; }
        set
        {
            _currentTree = value;
            //finishedRotateLeft = true;
            //finishedSearchForward = true;
            startSearch = false;
            shouldRotate = false;
            switch (_currentTree)
            {
                case BehaviorTree.AttackTree:
                    AI.behavior = AttackTree;
                    break;
                case BehaviorTree.WorkTree:
                    startPatrol = true;
                    if (PatrolPoints == null || PatrolPoints.Count == 0) PatrolPoints = setRandomPatrolPoints();
                    //Debug.Log("patrol");
                    AI.behavior = WorkTree;
                    break;
                case BehaviorTree.lookaroundTree:
                    startRandomLookaround();
                    AI.behavior = LookaroundTree;
                    break;
                case BehaviorTree.AllyFollowTree:
                    AI.behavior = AllyFollowTree;
                    break;
                case BehaviorTree.AllyWaitTree:
                    AI.behavior = AllyWaitTree;
                    break;
                case BehaviorTree.AllyGoToTree:
                    AI.behavior = AllyGoToTree;
                    break;
                case BehaviorTree.AllyActivateTree:
                    if (lastTree == BehaviorTree.AllyActivateTree)
                    {
                        //AI.paused = true;
                        CurrentTree = BehaviorTree.AllyFollowTree;
                        ability.endAllyAbility();
                    }else
                    {
                        if (ability.activateAllyAbility()) AI.behavior = AllyActivateTree;
                    }
                    break;
                case BehaviorTree.AllyAttackTree:
                    if (AttackTarget == null)
                    {
                        print("must have attackTarget set to enter attack tree. Try setting AttackTarget before setting CurrentTree");
                    }
                    AI.behavior = AllyAttackTree;
                    break;
                case BehaviorTree.ConfusionTree:
                    startConfusion = true;
                    AI.behavior = ConfusionTree;
                    break;
            }
            if (_currentTree != BehaviorTree.ConfusionTree) lastTree = _currentTree;
        }
    }

    private ENV_CrystalSpawner crystalSpawner;
    public ENV_CrystalSpawner CrystalSpawner
    {
        get { return crystalSpawner; }
    }

    protected bool _ghost;
    public bool isGhost
    {
        set {
            if (value == false) return;
            else ghostify();
        }
        get { return _ghost; }
    }

    public float myFollowDistance = 5f;

    // Use this for initialization
    public override void Awake()
    {
        base.Awake();
        //motor = gameObject.GetComponent<CHA_Motor>();
        agent = GetComponent<NavMeshAgent>();
        positionSensitivity = Mathf.Max(agent.stoppingDistance, 2);
        if(agent == null)
        {
            print("error: No NavMeshAgent Component");
        }
        AI = gameObject.GetComponent<BehaviorExecutor>();
        //healthBar = gameObject.GetComponentInChildren<UIX_CameraFacingBillboard_Minion>();
        
        //New health bars
        GameObject child = UTL_GameObject.cloneAtLocation("FloatingUI/BackgroundBar", transform.position);
        child.transform.parent = transform;

        //All minions need to be set to active for cleanse and purify to work properly
        State = "active";
    }
    // Path finding breaks if this is called in Awake()
    public override void Start()
    {
        base.Start();
        //isGhost = true;
        playerDriver = player.GetComponent<PLY_PlayerDriver>();
        //set start speed 
        agent.speed = normalSpeed;
        //patrolPos = transform.position;
        attackCooldown = 0f;
        crystalSpawner = GameObject.Find("Scripts").GetComponent<ENV_CrystalSpawner>();
        //THIS IS FOR TESTING ONLY
        //PatrolPoints = new List<Vector3>() { new Vector3(50, 50, 50), new Vector3(60, 60, 60) };
        if (PatrolPoints == null || PatrolPoints.Count == 0)
        {
            PatrolPoints = setRandomPatrolPoints();
            //PatrolPoints = new List<Vector3>() { transform.position };
        }

        //sets patrol points else wont start patrolling if STARTING in patrol state
        startPatrol = true;
        enemyWaitPosition = gameObject.transform.position;
        agent.SetDestination(transform.position);
        // initialize to full health
        CL = CL_MAX;
        ability = gameObject.GetComponent<ENT_MinionAbility>();

        // intialize the minion based on if they start as an enemy or ally
        if (startsAsAlly)
        {
            purify_self();
        }
        else
        {
            corrupt_self();
        }
    }

    public List<Vector3> setRandomPatrolPoints()
    {
        List<Vector3> pointsList = new List<Vector3>();
        for (int i = 0; i < NUMWANDERPOINTS; ++i)
        {
            float randomAngle = Random.Range(0, 355);
            float randomDist = Random.Range(5, 20);
            Vector3 randomPoint = new Vector3(Mathf.Sin(Mathf.Deg2Rad * randomAngle), 0, Mathf.Cos(Mathf.Deg2Rad * randomAngle));
            randomPoint *= randomDist;
            randomPoint += transform.position;
            pointsList.Add(randomPoint);
        }
        return pointsList;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (patrolTimer > 0 && (startPatrol || Vector3.Distance(transform.position, PatrolPoints[patrolIndex]) <= positionSensitivity)) patrolTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (CurrentTree == BehaviorTree.lookaroundTree)
        {
            if (searchTimer <= 0f)
            {
                CurrentTree = BehaviorTree.WorkTree;
            }
            else
            {
                searchTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            }
        }
        if (shouldRotate)
        {
            lookTowards(lookTowardsVec);
        }
        if (_ghost)
        {
            CL -= .7f * TME_Manager.getDeltaTime(TME_Time.type.enemies);
        }
        if (attackCooldown > 0)
        {
            attackCooldown = Mathf.Max(0F, attackCooldown - TME_Manager.getDeltaTime(TME_Time.type.enemies));
        }
        //if (AI.paused) AI.paused = false;
        
        if (purity && playerDriver.activeState == PLY_MoveState.type.surf)
        {
            agent.speed = surfingSpeed;
        }else
        {
            agent.speed = normalSpeed;
        }
        attackRechargeTime = normalAttackSpeed;
        //GetComponent<LineRenderer>().SetPosition(0, transform.position + new Vector3(0, 5, 0));
        //GetComponent<LineRenderer>().SetPosition(1, transform.position + new Vector3(0, 5, 0) + (transform.forward * 5));
        applyStatusEffects();
    }
    
    public override void activate()
    {

    }

    public ENT_DungeonEntity.monsterTypes getMyType()
    {
        return entity_type;
    }
    public GameObject getSpawnedBy()
    {
        return spawnedBy;
    }

    #region Behavior Tree Functions

    public void hangout()
    {
        agent.SetDestination(transform.position);
    }

    /*
    public void goToWork()
    {
        //This is temporary for testing purposes
        //This should be done when the minion is spawned, also update minion spawnedby and patroltarget there!!!!!!!!!!!!!!!!!!!!
        //monsterBrain.MyJob = AIC_Brain.work.gather;
        if (Vector2.Distance(endPoint, transform.position) > 3)
        {
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        else if (Vector2.Distance(workTarget.transform.position, transform.position) < 3)
        {
            //currently needs more logic in doWork() for this to work
            doWork();
            endPoint = spawnedBy.transform.position;
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        //defaults back to work target
        else
        {
            //currently needs more logic in doWork() for this to work
            doWork();
            endPoint = workTarget.transform.position;
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
    }*/

    public void gatherCrystal(GameObject crystal)
    {
        ENV_GroundCrystal gc = crystal.GetComponent<ENV_GroundCrystal>();
        if (gc == null)
        {
            print(gameObject.name + " tried to gatherCrystal on non-crystal object " + crystal.gameObject.name);
            return;
        }
        if (!crystalInRange(crystal))
        {
            print(gameObject.name + " is not within pickupRange of " + crystal.gameObject.name);
            return;
        }
        gc.takeCrystal(gameObject);
        //if near target pickup crystal and reassign target
        //if no target reassign target
        //nearestCrystal = crystalSpawner.getNearestCrystal(UTL_Math.vec3ToVec2(gameObject.transform.position));
    }

    public bool crystalInRange(GameObject crystal)
    {
        return Vector3.Distance(transform.position, crystal.transform.position) <= pickupRange;
    }

    //Don't use unless you are devon
    public virtual void patrol()
    {
        //if at current patrol point
        //move to next patrol point
        if (startPatrol || Vector3.Distance(transform.position, PatrolPoints[patrolIndex]) <= positionSensitivity)
        {
            if (patrolTimer <= 0)
            {
                //print("starting patrol");
                patrolIndex = ++patrolIndex;
                if (patrolIndex >= PatrolPoints.Count) patrolIndex = 0;
                goToPosition(PatrolPoints[patrolIndex]);
                //print("patrolIndex " + patrolIndex);
                startPatrol = false;
                patrolTimer = 3;
            }
        }
    }

    //Don't use unless you are devon
    public void traverseConfusedPath()
    {
        //if at current patrol point
        //move to next patrol point
        if (startConfusion || Vector3.Distance(transform.position, ConfusedPoints[confusedIndex]) <= positionSensitivity)
        {
            //print("starting patrol");

            confusedIndex = ++confusedIndex;
            if (confusedIndex >= ConfusedPoints.Count) confusedIndex = 0;
            goToPosition(ConfusedPoints[confusedIndex]);
            startConfusion = false;
        }
    }

    //deprecated
    public void goToRandomPosition()
    {
        
        goToNearbyPosition();
    }

    public void goToPlayer()
    {
        goToPosition(player.transform.position);
    }

    public bool CanSeePlayer()
    {
        //check if player is in sightRange before LOS check
        if (Vector3.Distance(gameObject.transform.position, player.transform.position) > sightRange) return false;
        return UTL_Dungeon.checkLOSWalls(transform.position, player.transform.position);
    }

    public bool isNearPlayer(float distance = 0)
    {
        if (distance == 0) distance = threatRange;
        return Vector3.Distance(transform.position, player.transform.position) < distance;
    }

    public void attackTarget(GameObject target)
    {
        if (attackCooldown > 0)
        {
            return;
        }
        //transform.rotation = Quaternion.LookRotation(target.transform.position);
        Vector3 vec = target.transform.position - transform.position;
        float magnitude = vec.magnitude;
        Vector3 dirToTarget = vec / magnitude;
        /*
        Vector3 heading = UTL_Math.angleToUnitVec(motor.getRotation());
        float dotProd = Vector3.Dot(dirToTarget, heading);*/
        if (!isFacing(target.transform.position))
        {
            //this means the target is behind the player
            lookTowards(target.transform.position);
            return;

            //maybe should force target to rotate to player?
        }
        GameObject projectile;
        if(purity) {
            projectile = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/AllyBasicAttack", gameObject.transform.position + dirToTarget * 1);
        }
        else
        {
            projectile = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/EnemyBasicAttack", gameObject.transform.position + dirToTarget * 1);
        }
        projectile.GetComponent<ABI_Projectile>().setDirection(dirToTarget);
        //projectile.GetComponent<CBI_CombatMessenger>().sender = gameObject;
        attackCooldown = attackRechargeTime;
        /*if (AttackDC != 0)
        {
            //projectile.GetComponent<CBI_CombatMessenger>().Damage_DC = AttackDC;
        }*/
    }

    public virtual bool isFacingForAttack(Vector3 pos)
    {
        pos.y = transform.position.y;
        float halfRad = sightRadius / 2;
        if (Vector3.Angle(transform.forward, pos - transform.position) < halfRad)
        {
            return true;
        }
        else return false;
    }

    //returns whether the pos is within the 180 degree area in front of the monster
    public virtual bool isFacing(Vector3 pos)
    {
        if (Vector3.Distance(transform.position, pos) < 3) return true;
        pos.y = transform.position.y;
        //minions can always detect in a small 360 radius
        float halfRad = sightRadius / 2;
        if (Vector3.Angle(transform.forward, pos - transform.position) < halfRad)
        {
            return true;
        }
        else return false;
    }

    public void goToNearbyPosition()
    {
        //distance of next point
        float distance = 10;
        goToPosition(getNearbyPosition(distance));
    }

    //Get random position whithin dist of gameObject
    public Vector3 getNearbyPosition(float dist)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += transform.position;

        randDirection = UTL_Dungeon.getTerrPosition(randDirection);

        return randDirection;
    }

    public bool isAtPosition(Vector3 pos, float reducedSensitivity = 0F)
    {
        //print("positionSensitivity " + positionSensitivity);
        
        bool result = Vector3.Distance(transform.position, pos) < (positionSensitivity + reducedSensitivity);
        //if (result) dontMove();
        return result;
    }

    public bool isMidpointNearTwig()
    {
        return allyManager.isMidpointNearTwig();
    }

    public bool amINearTwig()
    {
        return allyManager.amINearTwig(gameObject.transform.position);
    }

    public bool amINearSpawner(float distance = 10)
    {
        return Vector3.Distance(gameObject.transform.position, spawnedBy.transform.position) < distance;
    }

    public bool amINearSpawner()
    {
        try
        {
            return Vector3.Distance(gameObject.transform.position, spawnedBy.transform.position) < 10F;
        }
        catch
        {
            Debug.Log("Haro2D needs to fix this!");
            return false;
        }
    }

    public bool amINearWaitPos()
    {
        return Vector3.Distance(gameObject.transform.position, WaitPos) < myFollowDistance + positionSensitivity;
    }

    public bool isMidpointNearWaitPos()
    {
        return Vector3.Distance(allyManager.getMidpoint(), WaitPos) < allyManager.farFollowDistance;
    }
    #endregion

    public override void purify_self()
    {
        if (purifiable)
        {
            purity = true;
            CurrentTree = BehaviorTree.AllyFollowTree;
            //CurrentTree = BehaviorTree.ConfusionTree;
            allyManager.removeEnemy(gameObject);
            commandGroup = allyManager.addAlly(gameObject);
            //allyManager.setState(allyManager.getState());
            //gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.GetComponent<Image>().color = new Color32(14, 88, 27, 255);
            try
            {
                gameObject.transform.GetChild(1).GetComponent<FUI_PurityBar>().cl_bar.GetComponent<SpriteRenderer>().color = new Color32(14, 88, 27, 255);
            }
            catch
            {
                print("Purity bar is screwed up on " + gameObject.name);
            }

            //myFollowDistance = allyManager.getNextFollowDist;
            //ability.resetTimers();

            //Make this minion selectable
            gameObject.transform.parent = GameObject.Find("UnitContainer").transform;

        }
    }

    public override void corrupt_self()
    {
        purity = false;
        allyManager.removeAlly(gameObject);
        allyManager.addEnemy(gameObject);
        CurrentTree = BehaviorTree.WorkTree;
        //CurrentTree = BehaviorTree.lookaroundTree;
        //gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.GetComponent<Image>().color = new Color32(129,34,156,255);
        try {
            gameObject.transform.GetChild(1).GetComponent<FUI_PurityBar>().cl_bar.GetComponent<SpriteRenderer>().color = new Color32(129, 34, 156, 255);
        }
        catch
        {
            print("Purity bar is screwed up on " + gameObject.name);
        }
        //ability.resetTimers();
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        if (purity)
        {
            commandGroup.removeFromList(gameObject);
            print("removed from list");
            allyManager.removeAlly(gameObject);

        }
        else
        {
            allyManager.removeEnemy(gameObject);
        }
        UTL_GameObject.cloneAtLocation("Particles/CleanseEffect", transform.position);

        //Spawn Remnant at cleanse location
        if (!isGhost) {
            GameObject remnant;
            switch (entity_type)
            {
                case monsterTypes.flowerMinion:
                    remnant = UTL_GameObject.cloneAtLocation("Minions/Remnants/FlowerRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    break;
                case monsterTypes.mushroomMinion:
                    remnant = UTL_GameObject.cloneAtLocation("Minions/Remnants/MushroomRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    break;
                case monsterTypes.treeMinion:
                    remnant = UTL_GameObject.cloneAtLocation("Minions/Remnants/TreeRemnent", gameObject.transform.position);
                    remnant.GetComponent<ENT_Remnant>().entity_type = entity_type;
                    break;
            }
                
        }

        Destroy(gameObject);
    }

    public override void on_delta_clarity()
    {
        //healthBar.updateHealthBar(CL_MAX, CL);
    }

    public override void inflict_damage(float DC)
    {
        if (vulnerable)
        {
            CL -= DC;
            //if (CurrentTree == BehaviorTree.WorkTree) CurrentTree = BehaviorTree.lookaroundTree;
        }
    }

    public override void inflict_healing(float heal)
    {

    }

    public override void inflict_status(List<CBI_CombatMessenger.status> statuses)
    {
        //print("status inflicted");
        if (statuses == null) return;
        foreach (CBI_CombatMessenger.status stat in statuses)
        {
            float duration = CBI_CombatMessenger.statusCooldownMap[stat];
            bool makeNewStatus = true;
            foreach (Pair<CBI_CombatMessenger.status, float> curStat in StatusList)
            {
                if (stat==curStat.value1)
                {
                    curStat.value2 = duration;
                    makeNewStatus = false;
                    break;
                }
            }
            if (makeNewStatus) StatusList.Add(new Pair<CBI_CombatMessenger.status, float>(stat, duration));
        }
    }

    public override void attempt_purify_or_corrupt(CBI_CombatMessenger.ChangePurity purity)
    {

    }

    public override void receive_combat_message(CBI_CombatMessenger combat_message)
    {
        base.receive_combat_message(combat_message);
        if (combat_message.Damage_DC > 0 || combat_message.Status_list.Count > 0)
        {
            if (CurrentTree == BehaviorTree.WorkTree)
            {
                CurrentTree = BehaviorTree.lookaroundTree;
                startLookaround(combat_message.position - transform.position);
            }
        }
    }

    public void goToTargetCrystal()
    {
        if (crystal != null)
        {
            print(crystal.name);
            agent.SetDestination(crystal.transform.position);
        }
        else
        {
            print("No Crystal");
            agent.SetDestination(transform.position);
        }
    }

    //sets the crystal this minion is collecting from.
    private void setTargetCrystal(GameObject tc)
    {
        ENV_GroundCrystal gc = tc.GetComponent<ENV_GroundCrystal>();
        if (gc == null)
        {
            print("Cannot set " + tc.name + " as target crystal, its not a groundCrystal");
            return;
        }
        else if (_crystal == tc)
        {
            //wont do anything if the crystal is already targeted
            return;
        }
        //this increases the count of minionsCollecting if it is able as well as returning a value.
        else if (!gc.requestCrystalPickup())
        {
            print("Cannot set " + tc.name + " as target crystal, it already has too many minions collecting from it");
            return;
        }
        else if (_crystal != null)
        {
            _crystal.GetComponent<ENV_GroundCrystal>().releaseCrystalHold();
        }
        _crystal = tc;
    }

    public bool nearCrystal ()
    {
        if (crystal == null)
        {
            return false;
            //no pathable crystal
        }
        return Vector3.Distance(crystal.transform.position, transform.position) < pickupRange;
    }

    public void pickupCrystal()
    {
        if (crystal == null) return;
        crystal.GetComponent<ENV_GroundCrystal>().takeCrystal(gameObject);
    }

    public GameObject getNearestAlly()
    {
        return allyManager.getNearestAlly(gameObject.transform.position);
    }

    public GameObject getNearestEnemy()
    {
        return allyManager.getNearestEnemy(gameObject.transform.position);
    }

    public bool goToPosition(Vector3 position)
    {
        //Debug.Log("postion: "+position);
        bool result = agent.SetDestination(position);
        //if (!result) print("Error: " + gameObject.name + " can't get to " + position);
        //else print(gameObject.name + "going to " + position);
        //print(gameObject.name+ " endpoint: " + endPoint);
        return result;
    }


    protected void slowEffect()
    {
        agent.speed = slowSpeed;
        attackRechargeTime = slowAttackSpeed;
        return;
    }

    protected void confusionEffect()
    {
        if (CurrentTree != BehaviorTree.ConfusionTree)
        {
            ConfusedPoints = setRandomPatrolPoints();
            CurrentTree = BehaviorTree.ConfusionTree;
        }
    }

    protected void endConfusionEffect()
    {
        CurrentTree = lastTree;
    }

    protected void applyStatusEffects()
    {
        removeList.Clear();
        foreach (Pair<CBI_CombatMessenger.status, float> statPair in StatusList)
        {
            if (statPair.value2 <= 0)
            {
                removeList.Add(statPair);
                //StatusList.Remove(statPair);
                continue;
            }
            statPair.value2 -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
            switch (statPair.value1)
            {
                case CBI_CombatMessenger.status.slow:
                    slowEffect();
                    break;
                case CBI_CombatMessenger.status.confusion:
                    confusionEffect();
                    break;
            }
        }
        foreach(Pair<CBI_CombatMessenger.status, float> statToRemove in removeList)
        {
            StatusList.Remove(statToRemove);
            if (statToRemove.value1 == CBI_CombatMessenger.status.confusion) endConfusionEffect();
        }
    }

    public void startRandomLookaround ()
    {
        float startAngle = Random.Range(0, 355);
        Vector3 forwardVec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));
        startLookaround(forwardVec);
    }

    public void startLookaround(Vector3 forwardVec)
    {
        //USED TO ROTATATING MINION
        //lookaroundLeft = transform.right * -1;
        //lookaroundRight = transform.right;
        //print("left: " + lookaroundLeft + ", right: " + lookaroundRight);
        //finishedRotateLeft = false;

        //USED TO MOVE MINION IN SEARCH
        forwardVec = forwardVec.normalized;
        float startAngle = Vector3.Angle(Vector3.forward, forwardVec); 
        int DISTTOSEARCH = 10;
        startAngle = startAngle + 120 % 360;
        Vector3 rightvec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));
        startAngle = startAngle + 120 % 360;
        Vector3 leftVec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * startAngle), 0, Mathf.Cos(Mathf.Deg2Rad * startAngle));

        searchForward = getValidPath(UTL_Dungeon.getTerrPosition(transform.position + (DISTTOSEARCH * forwardVec)));
        searchLeft = getValidPath(UTL_Dungeon.getTerrPosition(transform.position + (DISTTOSEARCH * leftVec)));
        searchRight = getValidPath(UTL_Dungeon.getTerrPosition(transform.position + (DISTTOSEARCH * rightvec)));
        startSearch = true;
        searchTimer = SEARCH_TIME;
        //print("Search left: " + searchLeft + ", Search right: " + searchRight);
        finishedSearchLeft = false;
        finishedSearchForward = false;
    }

    public void rotate(bool rotateLeft, float speed =10f)
    {
        //CHANGE SPEED, DOES NOTHING
        if (rotateLeft)
        {
            gameObject.transform.Rotate(Vector3.down * speed, Space.Self);
            //gameObject.transform.Rotate(Vector3.up * speed, Space.Self);
        }
        else
        {
            gameObject.transform.Rotate(Vector3.up * speed, Space.Self);
        }
    }

    public void dontMove()
    {
        endPoint = gameObject.transform.position;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void ghostify()
    {
        _ghost = true;
        if (rend == null) rend = GetComponentInChildren<Renderer>();
        rend.material = ghostMat;
        CL = CL_MAX / 2; 
        //set material
        //set health to half of max health
    }


    public void searchingLeft()
    {
        searchLeft = UTL_Dungeon.getTerrPosition(getValidPath(searchLeft));
        goToPosition(searchLeft);
    }

    public void searchingRight()
    {
        searchRight = UTL_Dungeon.getTerrPosition(getValidPath(searchRight));
        goToPosition(searchRight);
    }

    public void searchingForward()
    {
        searchForward = UTL_Dungeon.getTerrPosition(getValidPath(searchForward));
        goToPosition(searchForward);
    }

    public void setValidPath (Vector3 pathEnd)
    {
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        Vector3 vec = pathEnd - transform.position;
        float dist = 2 * vec.magnitude;
        do
        {
            dist /= 2;
            vec = vec.normalized * dist;
            endPoint = transform.position + vec;
        }
        while (agent.SamplePathPosition(navmeshMask, dist, out hit));
    }

    public Vector3 getValidPath(Vector3 pathEnd)
    {
        int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
        NavMeshHit hit;
        Vector3 vec = pathEnd - transform.position;
        Vector3 result;
        float dist = 2 * vec.magnitude;
        do
        {
            dist /= 2;
            vec = vec.normalized * dist;
            result = transform.position + vec;
        }
        while (agent.SamplePathPosition(navmeshMask, dist, out hit));
        return result;
    }

    public void lookTowards(Vector3 lookAt)
    {

        if (Vector3.Angle(transform.forward, lookAt - transform.position) <= 5)
        {
            shouldRotate = false;
            return;
        }

        Vector3 direction = (lookAt - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, TME_Manager.getDeltaTime(TME_Time.type.enemies) * 2);


        //myFollowDistance = Vector3.Distance(player.transform.position, transform.position + lookDir);
        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDir), 0.1F);
    }
    

}