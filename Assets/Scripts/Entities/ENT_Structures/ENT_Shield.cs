﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ENT_Shield : ENT_Structure {
    public ENT_DungeonEntity entity_to_shield;
    bool applied_shield;
    public Material[] shield_materials = new Material[2];
    GameObject shield_effect;
    LineRenderer lineRenderer;
    //ParticleSystemRenderer shield_color;

    float timer = 0;


    // Use this for initialization
    public override void Start () {
        base.Start();
        State = "active";
        applied_shield = false;
        //shield_color = entity_to_shield.Shield.GetComponent<ParticleSystemRenderer>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        shield_entity();
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        update_shield_entity();
        //Debug.Log(entity_to_shield.shields);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.02f, gameObject.transform.position.z);
            timer += 0.02f;
            if (timer > 6)
            {
                Destroy(gameObject);
            }
        }
    }

    public void update_shield_entity()
    {
        if (State == "active")
        {
            try
            {
                if (entity_to_shield.shields >= 2)
                {
                    shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                    lineRenderer.material = lineRenderer.materials[1];
                }
                else if (entity_to_shield.shields == 1)
                {
                    shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                    lineRenderer.material = lineRenderer.materials[0];
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public void shield_entity()
    {
        if (!applied_shield && entity_to_shield != null)
        {
            //gameObject.GetComponent<LineRenderer>().SetPosition(0,new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z +7));
            //gameObject.GetComponent<LineRenderer>().SetPosition(1, new Vector3(entity_to_shield.transform.position.x, entity_to_shield.transform.position.y, entity_to_shield.transform.position.z -4));
            //gameObject.GetComponent<LineRenderer>().material.color = Color.cyan;
            if (entity_to_shield.shields <= 0)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                shield_effect = UTL_GameObject.cloneAtLocation("Environment Effects/Shield/Shield", entity_to_shield.transform.position);
                shield_effect.transform.SetParent(entity_to_shield.transform);

                try
                {
                    if (entity_to_shield.shields >= 2)
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                        lineRenderer.material = lineRenderer.materials[1];
                    }
                    else
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                        lineRenderer.material = lineRenderer.materials[0];
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }               
            }
            else if (entity_to_shield.shields >= 1)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                try
                {
                    foreach (Transform child in entity_to_shield.transform)
                    {
                        if (child.gameObject.tag == "shield")
                        {
                            shield_effect = child.gameObject;
                            break;
                        }
                    }
                    if (entity_to_shield.shields >= 2)
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[1];
                        lineRenderer.material = lineRenderer.materials[1];
                    }
                    else
                    {
                        shield_effect.GetComponent<ParticleSystemRenderer>().material = shield_materials[0];
                        lineRenderer.material = lineRenderer.materials[0];
                    }
                }
                catch (Exception e)
                {
                    throw new Exception("no ParticleSystem Renderer");
                }
            }
            lineRenderer.SetPosition(0, transform.position + new Vector3(0f, 5f, 0f));
            lineRenderer.SetPosition(1, entity_to_shield.transform.position);// + new Vector3(0f, 5f, 0f));
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        entity_to_shield.shields -= 1;
        applied_shield = false;
        if (entity_to_shield.shields <= 0)
        {
            Destroy(shield_effect);
        }
        Destroy(lineRenderer);
        State = "cleansed";
        rend.material = ghostMat;
    }
}
