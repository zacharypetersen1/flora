﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Furnace : ENT_Structure {

    List<GameObject> fireWalls;
    //Vector3 startPos;
    float timer = 0;

    //int ZoneIDtoActivate; // Could instead be direct reference to actual Zone object

    // Use this for initialization
    public override void Start () {
        base.Start();
        priority = 70; //Low priority is higher priority
        CL = CL_MAX;
        State = "active";
        //startPos = gameObject.transform.position;
        fireWalls = new List<GameObject>();
        for (int i = 0; i < gameObject.transform.childCount; ++i)
        {
            Transform child = gameObject.transform.GetChild(i);
            if (child.tag == "fireWall")
            {
                fireWalls.Add(child.gameObject);
            }
        }
    }

    // Update is called once per frame
    public override void Update () {
        base.Update();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.02f, gameObject.transform.position.z);
            timer += 0.02f;
            if (timer > 6)
            {
                Destroy(gameObject);
            }
            else if  (timer > 2)
            {
                foreach (GameObject wall in fireWalls)
                {
                    wall.GetComponent<Collider>().enabled = false;
                    wall.GetComponent<ParticleSystem>().Stop();
                }
            }
        }
    }

    public override void cleanse_self()
    {
        //Debug.Log("Furnace cleansed");
        base.cleanse_self();
        State = "cleansed";
        rend.material = ghostMat;
        //wallCollider.enabled = false;
        //Must set zonmanager to avoid error
        //ZON_Manager.zone_manager.zones[ZoneIDtoActivate].ActivateChildren();
    }
}
