﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Structure : ENT_DungeonEntity {

    /// <summary>
    /// Reference to zone that contains the structure
    /// </summary>
    public Zone ParentZone;
    public int priority; // How important it is for the player to target the structure
    public string JSON_path; // Where to find JSON data for the structure object
    public GameObject twig; // Reference to player object
    public int ID; //ID for each structure so that minions can know where they spawned from
    //public UIX_CameraFacingBillboard_Minion healthBar;

    // Use this for initialization
    public override void Start () {
        base.Start();
        //New health bars
        GameObject child = UTL_GameObject.cloneAtLocation("FloatingUI/BackgroundBar", transform.position);
        child.transform.parent = transform;
        allyManager.addStructure(gameObject);
        CL = CL_MAX;
        twig = player;
    }

    void subStart()
    {
        // Implementation in derived classes
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (State == "active" && CL <= 0)
        {
            cleanse_self();
        }
        subUpdate();
	}

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    void subUpdate()
    {
        // Implementation in derived classes
    }


    public void SpawnEnemy()
    {
        // Implementation in derived classes
    }

    //May not be necessary in this intermediate level class; implemented below this class in the hierarchy, interfaced above it.
    public override void activate()
    {

    }

    public override void on_delta_clarity()
    {
        //Old code
        //if (healthBar != null) {
        //    healthBar.updateHealthBar(CL_MAX, CL);
        //}
    }

    public override void inflict_damage(float DC)
    {
        base.inflict_damage(DC);
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        //GameObject healthBar = gameObject.transform.Find("BackgroundBar(Clone)").gameObject;
        Destroy(gameObject.transform.Find("BackgroundBar(Clone)").gameObject);
        allyManager.removeStructure(gameObject);
    }

        //private void OnTriggerEnter2D(Collider2D collision)
        //{
        //    try
        //    {
        //        CBI_CombatMessenger message = collision.gameObject.GetComponent<CBI_CombatMessenger>();
        //        receive_combat_message(message);
        //    }
        //    catch
        //    {
        //        Debug.Log("no combat message to recieve");
        //    }
        //}
    }
