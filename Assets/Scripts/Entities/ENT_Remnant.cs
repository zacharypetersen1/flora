﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Remnant : MonoBehaviour {
    public ENT_DungeonEntity.monsterTypes entity_type;
    float remnantTimer = 500F;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        remnantTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
		if(remnantTimer <= 0)
        {
            Destroy(gameObject);
        }
	}

    public GameObject revive(bool asAlly)
    {
        GameObject newMinion = UTL_Dungeon.spawnMinion(entity_type, transform.position, asAlly);
        newMinion.GetComponent<ENT_Body>().isGhost = true;
        return newMinion;
    }
}
