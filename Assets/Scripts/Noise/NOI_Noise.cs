﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NOI_Noise {



    //
    // returns noise value at given location
    //
    public abstract float getValue(float x, float y);
}
