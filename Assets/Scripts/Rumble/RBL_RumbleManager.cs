﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#

public class RBL_RumbleManager : MonoBehaviour {



    static RBL_RumbleManager singleton;
    Dictionary<string, RumbleModule> rumbleModules;



    public enum type
    {
        leftRumble, rightRumble
    };



    //
    // Holds everything for one type of rumble
    //
    private class RumbleModule
    {
        string name;
        public type rumbleType;
        bool isOnTimer = false;
        public float rumbleValue = 0;

        // constructor
        public RumbleModule(string setName, type setType)
        {
            name = setName;
            rumbleType = setType;
            TME_Timer.addTimer(name, TME_Time.type.master);     // TODO: add time type if want rumble to scale with time
        }

        // rumbles for specified amount of time
        public void rumbleOverTime(float value, float time)
        {
            isOnTimer = true;

            // set timer and update the rumble value
            TME_Timer.setTime(name, time);
            rumbleValue = value;
            recalculateRumble();
        }

        // sets rumble to specific value indefinately
        public void setRumble(float value)
        {
            isOnTimer = false;

            // update rumble value
            rumbleValue = value;
            recalculateRumble();
        }

        // runs once per frame while rumble is on timer
        public void update()
        {
            if (isOnTimer)
            {
                // if time is up, reset the rumble module to 0 rumble
                if (TME_Timer.timeIsUp(name))
                {
                    isOnTimer = false;
                    rumbleValue = 0;
                    recalculateRumble();
                }
            }
        }
    }



	// Use this for initialization
	void Awake () {
        singleton = this;
        rumbleModules = new Dictionary<string, RumbleModule>();
	}
	

    
    // start function
    void Start ()
    {
        // TODO expose this intialization code somewhere else
        addModule("treeRoots", type.rightRumble);
        addModule("basicCleanse", type.rightRumble);
        addModule("basicCleanseCharge", type.leftRumble);
        addModule("basicCleanseAOE", type.rightRumble);
        addModule("surfing", type.leftRumble);
        addModule("corruptSurfing", type.leftRumble);
        addModule("bumping", type.leftRumble);
    }



    // Update is called once per frame
    void Update () {

        // update each rumble module that is subscribed to the timer list
        foreach (var rumbleModule in rumbleModules.Values)
        {
            rumbleModule.update();
        }
	}



    //
    // Adds a rumble module
    //
    public static void addModule(string name, type rumbleType)
    {
        singleton.rumbleModules.Add(name, new RumbleModule(name, rumbleType));
    }



    //
    // Rumbles for a set amount of time
    //
    public static void rumbleOverTime(string moduleName, float rumbleValue, float time)
    {
        if (!(rumbleValue >= 0 && rumbleValue <= 1))
        {
            throw new UnityException("Invalid rumble value:" + rumbleValue + " , must be between [0,1]");
        }
        singleton.rumbleModules[moduleName].rumbleOverTime(rumbleValue, time);
    }



    //
    // turns rumble on indefinately
    //
    public static void turnRumbleOn(string moduleName, float rumbleValue)
    {
        if(!(rumbleValue > 0 && rumbleValue <= 1))
        {
            throw new UnityException("Invalid rumble value:" + rumbleValue + " , must be between (0,1]");
        }
        singleton.rumbleModules[moduleName].setRumble(rumbleValue);
    }



    //
    // turns rumble off
    //
    public static void turnRumbleOff(string moduleName)
    {
        singleton.rumbleModules[moduleName].setRumble(0);
    }



    //
    // Recalculates rumble values accounting for all rumble modules
    //
    static void recalculateRumble()
    {
        float tempLeft = 0;
        float tempRight = 0;

        // loop through each rumbleModule and accumulate thier values
        foreach (var rumbleModule in singleton.rumbleModules.Values)
        {
            switch (rumbleModule.rumbleType) {
                case type.leftRumble: tempLeft += rumbleModule.rumbleValue; break;
                case type.rightRumble: tempRight += rumbleModule.rumbleValue; break;
            }
        }

        // cap the values at 1
        if(tempLeft > 1)
        {
            tempLeft = 1;
        }
        if(tempRight > 1)
        {
            tempRight = 1;
        }

        // set the rumble to the new calculated values
        GamePad.SetVibration(0, tempLeft, tempRight);
    }
}