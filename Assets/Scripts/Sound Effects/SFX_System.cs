﻿// Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SFX_System : MonoBehaviour {

    protected AudioSource source;               // source that will play audio
    public float velocity;                      // movement speed of an object
    public GameObject audioSourceObject;        // object that source is to be attached to

    //
    // Initialize
    //
    void Start()
    {
        source = audioSourceObject.GetComponent<AudioSource>(); 
    }
}
