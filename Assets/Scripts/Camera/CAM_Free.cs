﻿//Zach
//Devon

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//
// Camera state when camera is centered around player and player can control it
//
public class CAM_Free : CAM_State
{
    private float slideSpeed = 0.5f;

    //
    // runs once per frame
    // Smoothly follows camera dolly position, "f" value controls slide speed
    //
    public override void FixedUpdate()
    {
		dolly.adjust();
		Transform target = dolly.getCamTransform();
        cameraScript.transform.position = Vector3.Lerp(cameraScript.transform.position, target.position, slideSpeed);
        cameraScript.transform.rotation = Quaternion.Lerp(cameraScript.transform.rotation, target.rotation, slideSpeed);

    }
}
