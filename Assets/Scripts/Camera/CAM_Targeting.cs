﻿//Devon

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Harold Townsend
//thanks to https://unity3d.com/learn/tutorials/projects/tanks-tutorial/camera-control?playlist=20081
//
// Camera state when camera is targeting an enemy or ability location
//
public class CAM_Targeting : CAM_State
{
    private float slideSpeed = 0.05f;
    private Vector3 center;
    private int targetCount = 0;
    private float greatestDistance = 0;
    private Vector3 camOffset;


    //need to set targets, find proper minSize and screenEdgeBuffer

    /*[HideInInspector]*/
    public List<Transform> targets = new List<Transform>();
    public float ScreenEdgeBuffer = 7f;
    public float MinSize = 5f;
    private Vector3 desiredPosition;
    private Quaternion desiredRotation;
    private float desiredSize;
    private Transform dollyTransform;
    private Camera cam;

    //initializes targeting camera
    //assumes both Camera and TargetedCameraDolly already exist
    public CAM_Targeting() : base()
    {
        cam = GameObject.Find("Camera").GetComponent<Camera>(); //expensive

        dollyTransform = targetDolly.transform;
        dollyTransform.position = dolly.transform.position;
        dollyTransform.rotation = dolly.transform.rotation;

        //delete when done testing
        GameObject p = GameObject.Find("Player");
        targets.Add(p.transform);
        GameObject test = GameObject.Find("TargetTestCube");
        targets.Add(test.transform);
    }

    public override void FixedUpdate()
    {

        Move();
        Zoom();
        Rotate();
        //Apply();
        if (slideSpeed < .7)
        {
            slideSpeed += (float)(.7 / 3) * Time.deltaTime;
        }
    }

    //move camera above average position
    private void Move()
    {
        dolly.adjust();
        //get average position (midpoint)
        desiredPosition = FindAveragePosition(targets); //change to FindMidpoint for different behavior
        //set cameraDolly position to midpoint
        dollyTransform.position = Vector3.Lerp(dollyTransform.position, desiredPosition, slideSpeed);
        //dollyTransform.position = desiredPosition;
        //Vector3 posA = targets[0].position;
        //Vector3 posB = targets[1].position;
        //targetDolly.setDirectionVector(posB, posA);
        Transform newCameraPosition = targetDolly.getTargetCamTransform();
        //Debug.Log(newCameraPositon.position);
        cameraScript.transform.position = Vector3.Lerp(cameraScript.transform.position, newCameraPosition.position, slideSpeed);
        cameraScript.transform.rotation = Quaternion.Lerp(cameraScript.transform.rotation, dolly.transform.rotation, slideSpeed);
    }

    //rotate cam to look at midpoint
    private void Rotate()
    {
        //change this to something cool later
        //for testing this just makes the camera look straight down
        //cam.transform.LookAt(dollyTransform);
        //set camera to proper rotation
    }

    //calculates and applies zoom. Technically zoom is applied to the next frame :P
    private void Zoom()
    {
        //find proper zoom size
        desiredSize = getRequiredSize(targets);
        targetDolly.directionMag = getRequiredDistance(desiredSize);
        //cam.fieldOfView = desiredSize*10;
        //cam.transform.localScale = new Vector3(desiredSize, desiredSize, desiredSize);
        //set camera size to proper zoom size
    }

    //returns midpoint of two furthest apart targets
    private Vector3 FindMidpoint(List<Transform> targets)
    {
        Vector3 midpoint = new Vector3();
        float maxDist = 0f;
        for (int i = 0; i < targets.Count; ++i)
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                if (i == j) continue;
                Vector3 posA = targets[i].position;
                Vector3 posB = targets[j].position;
                float dist = Vector3.Distance(posA, posB);
                if (dist > maxDist)
                {
                    maxDist = dist;
                    midpoint = (posA + posB) / 2;
                    midpoint.z = 0;
                }
            }

        }
        return midpoint;
    }

    //returns midpoint of all targets
    private Vector3 FindAveragePosition(List<Transform> targets)
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;
        for (int i = 0; i < targets.Count; ++i)
        {
            //Debug.Log(targets[i]);
            averagePos += targets[i].position;
            ++numTargets;
        }
        if (numTargets > 0)
            averagePos /= numTargets;

        //y value should not be divided by numTargets
        averagePos.z = 0;
        return averagePos;
    }


    private float getRequiredSize(List<Transform> targets)
    {
        float greatestDistance = MinSize;
        for (int i = 0; i < targets.Count; ++i)
        {
            for (int j = 0; j < targets.Count; ++j)
            {
                if (i == j) continue;
                float dist = Mathf.Abs(targets[i].position.x - targets[j].position.x) / cam.aspect;
                if (dist > greatestDistance) greatestDistance = dist;
                dist = Mathf.Abs(targets[i].position.y - targets[j].position.y);
                if (dist > greatestDistance) greatestDistance = dist;
            }
        }
        return greatestDistance + ScreenEdgeBuffer;
    }
    //not used
    private float getRequiredDistance(float requiredSize)
    {
        return requiredSize * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
    }
}