﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EFX_SlideIn : MonoBehaviour
{
    public bool right_enter = true; // Enter screen from right, otherwise enter from left
    public float scroll_speed = 1; // Speed of scrolling, can use for parallax
    // Final position relative to camera
    public float x_offset = 0;
    public float y_offset = 0;
    public float z_offset = 0; // Use to arrange multiple elements, lower is closer
    public float max_scroll_offset = 1;
    float scroll_offset = 0;
    float counter = 0;
    float offscreen_offset = 3;
    bool active = false;
    bool slideOut = false;
    Vector3 init_p;
    // Use this for initialization
    void Start()
    {
        if (right_enter) init_p = new Vector3(offscreen_offset - x_offset, y_offset, 1 + z_offset);
        else init_p = new Vector3(x_offset - offscreen_offset, y_offset, 1);
        new Vector3(x_offset - offscreen_offset, y_offset, 1 + z_offset);
        transform.localPosition = init_p;
        max_scroll_offset = max_scroll_offset * scroll_speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (INP_PlayerInput.getButtonDown("Story"))
        {
           
            if (active)
            {
                active = false;
                slideOut = true;
                TME_Manager.setTimeScalar(TME_Time.type.game, 1);
            }
            else
            {
                active = true;
                slideOut = false;
                TME_Manager.setTimeScalar(TME_Time.type.game, 0);
            }
        }
        if (active)
        {
            set_position(counter);
            if (counter < 1) counter += .02f;
            // Scrolling functionality
            // Does not work

        }
        if (slideOut)
        {
            set_position(counter);
            if (counter > 0) counter -= .02f;
        }
    }
    public void set_position(float c)
    {
        Debug.Log("AYYLMAO");
        scroll_offset -= Input.GetAxis("Mouse ScrollWheel") * scroll_speed;
        scroll_offset = Mathf.Max(0, scroll_offset);
        scroll_offset = Mathf.Min(max_scroll_offset, scroll_offset);
        var cam_p = transform.parent.transform.position;
        var x = easeOut(c);
        Vector3 tar_p;
        if (right_enter) tar_p = new Vector3(offscreen_offset * (1 - x) - x_offset, y_offset + scroll_offset, 1 + z_offset);
        else tar_p = new Vector3(x_offset - offscreen_offset * (1 - x), y_offset + scroll_offset, 1 + z_offset);
        var tar_p_lerped = Vector3.Lerp(transform.localPosition, tar_p, Time.deltaTime * 8);
        transform.localPosition = tar_p_lerped;

        // var scrolled_p = new Vector3(transform.localPosition.x, transform.localPosition.y + scroll_offset, transform.localPosition.z);
        //transform.localPosition = Vector3.Lerp(transform.localPosition, scrolled_p, Time.deltaTime * 5);
    }
    public static float easeOut(float x)
    {
        return -(2.5f) * Mathf.Pow(x, 3) + (3.5f) * Mathf.Pow(x, 2);
    }
}