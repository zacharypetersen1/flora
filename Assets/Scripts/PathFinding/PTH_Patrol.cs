﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PTH_Patrol : MonoBehaviour
{


    CHA_Motor motor;
    Vector3 pastPos;
    PTH_Path path;
    Vector2 endPoint;
    public GameObject partolTarget;
    int patrolPosition = 1;

    GameObject[] rock_polygons;
    GameObject[] dirt_polygons;

    // Use this for initialization
    void Awake()
    {
        motor = gameObject.GetComponent<CHA_Motor>();

        rock_polygons = GameObject.FindGameObjectsWithTag("rockWall");
        dirt_polygons = GameObject.FindGameObjectsWithTag("dirtWall");
    }
    // Path finding breaks if this is called in Awake()
    private void Start()
    {
        string pointName = "PatrolPoint" + patrolPosition.ToString();
        //Debug.Log(name + " " + pointName);
        Vector2 patrolPoint = UTL_Math.vec3ToVec2(partolTarget.transform.Find(pointName).position);
        endPoint = patrolPoint;

        //path = PTH_Finder.findPath(transform.position, targetTransform.position);
        path = PTH_Finder.findPath(transform.position, endPoint);
        //pastPos = targetTransform.position;
        pastPos = endPoint;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        string pointName = "PatrolPoint" + patrolPosition.ToString();
        //Debug.Log(name + " " + pointName);
        Vector2 patrolPoint = UTL_Math.vec3ToVec2(partolTarget.transform.Find(pointName).position);
        if (Vector2.Distance(endPoint, UTL_Math.vec3ToVec2(transform.position)) > 2)
        {
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        else
        {
            ++patrolPosition;
            if (patrolPosition > partolTarget.transform.childCount)
            {
                patrolPosition = 1;
            }
            pointName = "PatrolPoint" + patrolPosition.ToString();
            patrolPoint = UTL_Math.vec3ToVec2(partolTarget.transform.Find(pointName).position);
            endPoint = patrolPoint;
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        if (path != null)
        {
            motor.moveAlongPath(path);
        }
    }
}
