﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPathing : MonoBehaviour {

    Transform targetTransform;
    CHA_Motor motor;
    Vector3 pastPos;
    PTH_Path path;
    Vector2 endPoint;

    GameObject[] rock_polygons;
    GameObject[] dirt_polygons;

    // Use this for initialization
    void Start () {

        //targetTransform = GameObject.Find("end").transform;
        motor = gameObject.GetComponent<CHA_Motor>();

        
        rock_polygons = GameObject.FindGameObjectsWithTag("rockWall");
        dirt_polygons = GameObject.FindGameObjectsWithTag("dirtWall");

        endPoint = generateVector2();
        //endPoint = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));

        //path = PTH_Finder.findPath(transform.position, targetTransform.position);
        path = PTH_Finder.findPath(transform.position, endPoint);
        //pastPos = targetTransform.position;
        pastPos = endPoint;
    }
	
	// Update is called once per frame
	void Update () {
        /*
        if(targetTransform.position != pastPos)
        {
            path = PTH_Finder.findPath(transform.position, targetTransform.position);
        }
  
        motor.moveAlongPath(path);

        pastPos = targetTransform.position;
        */

        if((UTL_Math.vec3ToVec2(transform.position) - endPoint).magnitude <= 2)
        {
            endPoint = generateVector2();
            //endPoint = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));
            path = PTH_Finder.findPath(transform.position, endPoint);
        }
        motor.moveAlongPath(path);
    }

    public Vector2 generateVector2()
    {
        Vector2 point;
        do
        {
            point = new Vector2(Random.Range(0f, 50f), Random.Range(0f, 50f));
        } while (checkPointinCollider(point) == true);
        return point;
    }

    public bool checkPointinCollider(Vector2 point)
    {
        foreach(GameObject poly in dirt_polygons)
        {
            if (poly.GetComponent<PolygonCollider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        foreach (GameObject poly in rock_polygons)
        {
            if (poly.GetComponent<PolygonCollider2D>().OverlapPoint(point))
            {
                return true;
            }
        }
        return false;
    }
}
