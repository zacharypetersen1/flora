﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBI_CombatMessenger {

    public float Damage_DC = 0;
    public float Healing_DC = 0;
    public ChangePurity changePurity = ChangePurity.dontChange;
    public List<status> Status_list = null;
    public Vector3 position;
    //public List<string> Valid_target_tags = null;
    //public GameObject Sender = null;

    public CBI_CombatMessenger(float dc, float healing, ChangePurity purify, List<status> statList, Vector3 positionOfMessage/*, GameObject sender*/)
    {
        Damage_DC = dc;
        Healing_DC = healing;
        changePurity = purify;
        Status_list = statList;
        position = positionOfMessage;
        //Sender = sender;
    }

    //put all statuses here
    public enum ChangePurity
    {
        dontChange, purify, corrupt
    }

    
    public enum status
    {
        slow, confusion
    }

    public static Dictionary<status, float> statusCooldownMap = new Dictionary<status, float>()
    {
        {status.slow, 5F },
        {status.confusion, 5F }
    };

    public static void sendCombatMessage (ENT_DungeonEntity target, CBI_CombatMessenger message)
    {
        target.receive_combat_message(message);
    }

    /*public void init(float dc, float healing, string purify, List<status> statuses, GameObject sender)
    {
        this.Damage_DC = dc;
        this.Healing_DC = healing;
        this.Purify_or_Corrupt = purify;
        this.Status_list = statuses;
        this.sender = sender;
    }*/




    //
    // Runs on collision detection
    //
    /*
    private void OnTriggerEnter(Collider col)
    {
        // loop through all valid targets
        foreach (var tag in Valid_target_tags)
        {

            // see if we collided with a valid target
            if (col.tag == tag)
            {
                // initiate combat interation
                if ((sender.GetComponent<ENT_Body>().Purity==false && col.tag=="Player") ||sender.GetComponent<ENT_Body>().Purity != col.GetComponent<ENT_Body>().Purity)
                {
                    col.GetComponent<ENT_DungeonEntity>().receive_combat_message(this);
                    //Destroy(gameObject);
                    return;
                }
            }
        }
    }

    private void OnTriggerStay(Collider col)
    {
        // loop through all valid targets
        foreach (var tag in Valid_target_tags)
        {

            // see if we collided with a valid target
            if (col.tag == tag)
            {
                // initiate combat interation
                if (sender.GetComponent<ENT_Body>().Purity != col.GetComponent<ENT_Body>().Purity)
                {
                    col.GetComponent<ENT_DungeonEntity>().receive_combat_message(this);
                    return;
                }
            }
        }
    }*/
}
