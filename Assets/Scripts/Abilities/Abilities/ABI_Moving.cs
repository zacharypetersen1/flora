﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Moving : ABI_Ability {



    float surfRumbleValue = 0.5f;
    float surfRechargeTime = 0.4f;
    bool isSurfing = false;
    float minVelocity = 0.12f;
    float decelRate = .1f;
    float acelRate = .25f;
    
    // FMOD parameters
	[FMODUnity.EventRef]
    float corruptVal = 0;
    float corruptValInterp = 0.003f;
    float boostVal = 0;
    float boostValInterp = 0.01f;

    ParticleSystem.EmissionModule emNormal;
    ParticleSystem.EmissionModule emCorrupt;



    GameObject bumpObj;
    
    //
    // constructor
    //
    public ABI_Moving(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
        TME_Timer.addTimer("surfTimer", TME_Time.type.player);
    }



    //
    // Init
    //

    //Warning! this never runs!
    public override void start()
    {
        bumpObj = GameObject.Find("PlayerBumper");
        //Debug.Log("bumpObj" + bumpObj);
        bumpObj.SetActive(false);
        emNormal = GameObject.Find("SurfParticle").GetComponent<ParticleSystem>().emission;
        emCorrupt = GameObject.Find("SurfParticleCorrupt").GetComponent<ParticleSystem>().emission;
    }



    //
    // Update
    //
    public override void update()
    {
        if (bumpObj == null) bumpObj = GameObject.Find("PlayerBumper");

        // Fade music track for surfing
        boostVal += isSurfing ? boostValInterp : -boostValInterp;
        boostVal = Mathf.Clamp(boostVal, 0, 1);
        if (MUS_System.singleton != null) MUS_System.singleton.MusicBoost(boostVal);

        // Fade music track for corruption
        corruptVal += MAP_NatureMap.natureTypeAtPos(driver.transform.position) == MAP_NatureData.type.corrupt ? corruptValInterp : -corruptValInterp*2;
        corruptVal = Mathf.Clamp(corruptVal, 0, 1);
        if (MUS_System.singleton != null) MUS_System.singleton.MusicCorrupt(corruptVal);
    }



    //
    // activate TreeRoots
    //
    public override void buttonDown()
    {
        if (driver.sensingDirtWall)
        {
            // go into "TreeRoot" state in player's finite state machine
            driver.setState(PLY_MoveState.type.treeroots);
            manager.reduceAbilityCharge(ABI_Manager.abilityType.Moving);
        }
    }



    //
    // Runs once when button is released
    //
    public override void buttonUp()
    {
        if (isSurfing)
        {
			stopSurfing();
			MUS_System.singleton.surfSoundTrue(0);
        }
    }



    //
    // Runs once when button is first detected as "Held"
    //
    public override void buttonHeld()
    {
        
    }



    //
    // Runs once per frame after button is detected as "Held"
    //
    public override void buttonHeldDown(float holdTime)
    {
        //Debug.Log("buttonHeldDown");
        if (!isSurfing && ableToSurf())
        {
            //Debug.Log("shouldStartSurfing");
			startSurfing();
			MUS_System.singleton.surfSoundStop ();
			MUS_System.singleton.surfSoundStart ();
			MUS_System.singleton.surfSoundTrue(1);
			MUS_System.singleton.surfSoundSpeed(0);
        }

        if (isSurfing && ableToSurf())
        {
            setSurfingSpeed();
        }

        if (isSurfing && !ableToSurf())
        {
			stopSurfing();
			MUS_System.singleton.surfSoundTrue(0);
        }
    }



    //
    // Checks to see if the player can surf
    //
    public bool ableToSurf()
    {
        return onSurfableGround()
            && driver.activeState != PLY_MoveState.type.treeroots 
            && TME_Timer.timeIsUp("surfTimer");
    }



    //
    // Checks to see if the player is currently on surfable ground
    //
    public bool onSurfableGround()
    {
        return MAP_NatureMap.natureTypeAtPos(driver.transform.position) != MAP_NatureData.type.rock;
    }



    //
    // Checks to see if the player is currectly moving fast enough to maintain surfing
    //
    public bool atSurfableVelocity()
    {
        return motor.getVelocityMagnitude() > minVelocity;
    }



    //
    // Start surfing
    //
    public void startSurfing()
    {
        driver.setState(PLY_MoveState.type.surf);
        if (bumpObj != null) bumpObj.SetActive(true);
        //CAM_Camera.setDollyInfluence(1);
        RBL_RumbleManager.rumbleOverTime("surfing", surfRumbleValue, 0.15f);
        motor.animObject.GetComponent<Animator>().SetTrigger("startSurfing");
        if (MAP_NatureMap.natureTypeAtPos(driver.transform.position) == MAP_NatureData.type.corrupt) emNormal.enabled = true;
        else emCorrupt.enabled = true;
        isSurfing = true;
        //motor.speed = driver.surfingSpeed;
        setSurfingSpeed();
        motor.rotationVelocity = driver.surfingRotVelMax;
        motor.clampVelocityToDirection = true;
        //Debug.Log("Got to end of startSurfing");
    }



    //
    // Sets twig's speed based on ground type
    //
    private void setSurfingSpeed()
    {
        if (MAP_NatureMap.natureTypeAtPos(driver.transform.position) == MAP_NatureData.type.grass)
        {
            emNormal.enabled = true;
            emCorrupt.enabled = false;
            RBL_RumbleManager.turnRumbleOff("corruptSurfing");
            if (motor.speed < driver.surfingSpeed)
            {
                if (motor.speed == driver.corruptSurfingSpeed) RBL_RumbleManager.rumbleOverTime("surfing", .5f, 0.15f);
                motor.speed += acelRate;
                if (motor.speed > driver.surfingSpeed) motor.speed = driver.surfingSpeed;
            }
        }
        else
        {
            emNormal.enabled = false;
            emCorrupt.enabled = true;
            RBL_RumbleManager.turnRumbleOn("corruptSurfing", .15f);
            if (motor.speed > driver.corruptSurfingSpeed)
            {
                motor.speed -= decelRate;
                if (motor.speed < driver.corruptSurfingSpeed) motor.speed = driver.corruptSurfingSpeed;
            }
        }
    }


    //
    // Stop surfing
    //
    public void stopSurfing()
    {
        driver.setState(PLY_MoveState.type.normal);
        if (bumpObj != null) bumpObj.SetActive(false);
        //CAM_Camera.setDollyInfluence(0);
        RBL_RumbleManager.rumbleOverTime("surfing", surfRumbleValue, 0.1f);
        RBL_RumbleManager.turnRumbleOff("corruptSurfing");
        TME_Timer.setTime("surfTimer", surfRechargeTime);
        motor.animObject.GetComponent<Animator>().SetTrigger("stopSurfing");
        emNormal.enabled = false;
        emCorrupt.enabled = false;
        isSurfing = false;
        motor.speed = driver.normalSpeed;
        motor.rotationVelocity = driver.normalRotVelocity;
        motor.clampVelocityToDirection = false;
        
    }



    //
    // overwrite the held threshold to a low value
    //
    public override float getHeldThreshold()
    {
        return 0.15f;
    }



    //private void disableCorruptParticles()
    //{
    //    ParticleSystem.EmissionModule em = GameObject.Find("SurfParticle_Particle").GetComponent<ParticleSystem>().emission;
    //}
}
