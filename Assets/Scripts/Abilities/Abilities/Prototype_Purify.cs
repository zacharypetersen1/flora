﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_Purify : ABI_Ability
{


    //This is how much purity progress is lost each frame on enemies whose purification has been interrupted.
    private float purityDecay = 0.006f;
    //This is how fast the player makes purification progress on the target they are purifying.
    private float purityGrowth = 0.03f;
    private float Delta = 0;

    //This temporary variable keeps track of who is currently being purified
    private ENT_Body entityToPurify;
    public ENT_Body target_body;
    private float goal_CL = 0;

    //This dict keeps track of all entities' purity progress. This way, you can pick up
    //roughly where you left off on purifying a minion if you get interrupted.
    Dictionary<ENT_Body, float> purityProgresssDict = new Dictionary<ENT_Body, float>();

    //
    // constructor
    //
    public Prototype_Purify(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
        //Debug.Log("Prototype constructed.");
    }


    //Called every frame
    public override void update()
    {
        Delta = Time.deltaTime;
        
        //For prototye, change to only called when entity enters or leaves some radius.
        //For final build, change to call when player presses the target button
        GameObject check_target = UTL_Targeting.target;
        if (check_target != null)
        {
            target_body = check_target.GetComponent<ENT_Body>();
        }
        if (INP_PlayerInput.getButtonDown("Purify"))
        {
            buttonDown();
            Delta = 0;
        }
        if (INP_PlayerInput.getButtonUp("Purify"))
        {
            buttonUp();
        }
        //If target changes, cancel purification
        if (entityToPurify != null && target_body != entityToPurify && UTL_Targeting.target != null)
        {
            CancelPurification();
        }
        List<ENT_Body> keys = new List<ENT_Body> (purityProgresssDict.Keys);
        foreach (var key in keys)
        {
            if (purityProgresssDict[key] > 0)
            { //Decay all values except the ones already at 0, even the target being purified.
              //The target being purified has extra growth above to counteract this decay.
                purityProgresssDict[key] -= purityDecay;
            }
            if (purityProgresssDict[key] < 0) purityProgresssDict[key] = 0; //Snap to 0
            else
            {
                //purityProgresssDict[key].
            }
        }
        buttonHeldDown(Delta);
    }


    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        if (target_body == null)
        {
            Debug.Log("No target within range. Nothing to purify.");
        }
        //If button has just been pressed, start Purifying
        else
        {
            try
            {
                BeginPurify(target_body);
            }
            catch
            {
                Debug.Log("Tried to purify something that wasn't a DungeonEntity");
            }
        } 
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        //If button is let go, cancel purification
        CancelPurification();
    }


    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown(float holdTime)
    {
        //If button is being held, do Purifying
        if (entityToPurify != null && UTL_Targeting.target != null)
        {
            purityProgresssDict[entityToPurify] += purityGrowth;
            purityProgresssDict[entityToPurify] += purityDecay; //To counteract decay.
                                                                //If successfully finished purifying, call CompletePurification.
            if (purityProgresssDict[entityToPurify] >= goal_CL)
            {
                CompletePurification();
            }
        }
    }

    void BeginPurify(ENT_Body objectToPurify)
    {
        Debug.Log("Begin purify");
        if (!purityProgresssDict.ContainsKey(objectToPurify))
        { //If we have never made any purification progress on this entity, add it to the dict
            purityProgresssDict[objectToPurify] = 0;
        }
        //Get information about the target
        goal_CL = objectToPurify.CL;
        //Purity = purityProgresssDict[objectToPurify]
        entityToPurify = objectToPurify;
    }

    void CancelPurification()
    {
        Debug.Log("Cancel puify");
        entityToPurify = null;
        //CAM_Camera.setState(CAM_State.type.returning);
        //UTL_Targeting.target = null;
    }

    void CompletePurification()
    {
        Debug.Log("Complete purify");
        //Trigger the target's onPurify event.
        entityToPurify.purify_self();
        CAM_Camera.setState(CAM_State.type.returning);
        entityToPurify = null;
        UTL_Targeting.target = null;
    }

}
