﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * MISNOMER: THIS SCRIPT IS ACTUALLY FOR PURIFYING NOT CLEANSING
 */
public class ABI_Cleansing : ABI_Ability {

    //This is how much purity progress is lost each frame on enemies whose purification has been interrupted.
    private float purityDecay = 0.01f;
    //This is how fast the player makes purification progress on the target they are purifying.
    private float purityGrowth = 0.3f;
    private float Dist = 0f;

    //These temporary variables keeps track of who is currently being purified and references its body
    private GameObject entityToPurify;
    private ENT_Body body;

    private List<ENT_DungeonEntity> allMinions;

    //This dict keeps track of all entities' purity progress. This way, you can pick up
    //roughly where you left off on purifying a minion if you get interrupted.
    Dictionary<GameObject, Vector2> purityProgresssDict = new Dictionary<GameObject, Vector2>();

    //
    // constructor
    //
    public ABI_Cleansing(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
        allMinions = new List<ENT_DungeonEntity>();
    }

    public override void start()
    {
        //List<GameObject> allAllies = player.GetComponent<PLY_AllyManager>().getAllAllies;
        //List<GameObject> allEnemies = player.GetComponent<PLY_AllyManager>().getAllEnemies;
        ////Fill allMinions with DungeonEntity versions of all allies and enemies
        //foreach(GameObject ally in allAllies)
        //{
        //    allMinions.Add(ally.GetComponent<ENT_DungeonEntity>());
        //}
        //foreach (GameObject enemy in allEnemies)
        //{
        //    allMinions.Add(enemy.GetComponent<ENT_DungeonEntity>());
        //}
        ////Give them all healthbars!
        //Debug.Log("Minions should eventually have the new health bars in their prefabs.");
        //foreach (ENT_DungeonEntity ent in allMinions)
        //{
        //    GameObject child = UTL_GameObject.cloneAtLocation("FloatingUI/BackgroundBar", ent.transform.position);
        //    child.transform.parent = ent.transform;
        //}
    }

    //Called every frame
    public override void update()
    {
        //Update all health bars
        updateAllHealthBars();
        //If target changes, cancel purification
        if ((entityToPurify != null && UTL_Targeting.target != entityToPurify && UTL_Targeting.target != null) || CAM_Dolly.locked)
        {
            CancelPurification();
        }
        List<GameObject> keys = new List<GameObject>(purityProgresssDict.Keys);
        foreach (var key in keys)
        {
            if (purityProgresssDict[key][0] >= 0)
            { //Decay all values except the ones already at 0, even the target being purified.
              //The target being purified has extra growth to counteract this decay.
                purityProgresssDict[key] -= new Vector2(purityDecay,0);
            }

            if (purityProgresssDict[key][0] < 0)
            { //If purity progress has completely decayed from a minion, remove it from the dictionary
                key.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().hidePurity();
                purityProgresssDict.Remove(key);
            }
            else if(key)
            {
                float missing_purity = purityProgresssDict[key][1] - purityProgresssDict[key][0];
                key.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().scalePurityToValue(missing_purity, purityProgresssDict[key][1]);
            }
        }
    }


    //
    // Runs once when ability is first activated;
    //
    public override void buttonDown()
    {
        //Don't purify if left clicking in select mode
        if (!GameObject.Find("SelectionManager").GetComponent<SelectionManager>().selectMode)
        {
            //Get current target
            GameObject check_target = UTL_Targeting.target;
            //If no current target update targeting
            if (!check_target)
            {
                check_target = UTL_Targeting.getTarget();
            }
            //If target found change camera state
            if (check_target)
            {
                CAM_Camera.setState(CAM_State.type.targeting);
            }
            //Attempt to purify target
            if (check_target != null)
            {
                try
                {
                    BeginPurify(check_target);
                }
                catch
                {
                    Debug.Log("Tried to purify something that wasn't a DungeonEntity");
                }
            }
        }
    }



    //
    // Runs once when ability is first released
    //
    public override void buttonUp()
    {
        //If button is let go, cancel purification
        if (entityToPurify)
        {
             CancelPurification();
        }  
    }


    //
    // Runs once per frame while ability is being held down
    //
    public override void buttonHeldDown(float holdTime)
    {
        if (entityToPurify && !purityProgresssDict.ContainsKey(entityToPurify))
        {
            BeginPurify(entityToPurify);
        }
        //If button is being held, do Purifying
        if (entityToPurify != null && UTL_Targeting.target != null)
        {
            Dist = Vector3.Distance(player.transform.position, entityToPurify.transform.position);
            purityProgresssDict[entityToPurify] += new Vector2((purityGrowth / Dist), 0);
            purityProgresssDict[entityToPurify] += new Vector2((purityDecay), 0); //To counteract decay.
            //If successfully finished purifying, call CompletePurification.
            if (purityProgresssDict[entityToPurify][0] >= purityProgresssDict[entityToPurify][1])
            {
                CompletePurification();
            }
        }
    }

    void BeginPurify(GameObject objectToPurify)
    {
        //Debug.Log("Begin purify");
        body = objectToPurify.GetComponent<ENT_Body>();
        objectToPurify.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().showPurity();
        if (!purityProgresssDict.ContainsKey(objectToPurify))
        { //If we have never made any purification progress on this entity, add it to the dict
            purityProgresssDict[objectToPurify] = new Vector2(purityGrowth, body.CL);
        }
        else
        { //Updates stored CL value if entity already in dict
            purityProgresssDict[objectToPurify] = new Vector2(purityProgresssDict[objectToPurify][0], body.CL);
        } //Note Current Purity = purityProgresssDict[objectToPurify][0]
        entityToPurify = objectToPurify;
        body.vulnerable = false;
    }

    void CancelPurification()
    {
        //Debug.Log("Cancel puify");
        if (CAM_Camera.singleton.currentState == CAM_State.type.targeting)
        {
            CAM_Camera.setState(CAM_State.type.returning);
        }
        if (body && entityToPurify)
        {
            body.vulnerable = true;
            entityToPurify = null;
            body = null;
        }
    }

    void CompletePurification()
    {
        //Debug.Log("Complete purify");
        //Trigger the target's purify_self event.
        body.purify_self();
        body.CL = body.CL_MAX;
        body.vulnerable = true;
        CAM_Camera.setState(CAM_State.type.returning);
        var children = entityToPurify.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            //Remove crystals
            if (child.name.StartsWith("Crystal"))
            {
                UnityEngine.Object.Destroy(child.gameObject);
            }
        }
        //Remove purity progress bar
        entityToPurify.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().scalePurityToValue(0.0f, 1.0f);
        //Remove from dictionary and reset temporary variables
        purityProgresssDict.Remove(entityToPurify);
        entityToPurify = null;
        body = null;
        UTL_Targeting.target = null;
    }

    //May be moved in the future...
    //Updates the values of all health bars
    public void updateAllHealthBars()
    {
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllAllies)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllEnemies)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
        foreach (GameObject ent in player.GetComponent<PLY_AllyManager>().getAllStructures)
        {
            ENT_DungeonEntity dent = ent.GetComponent<ENT_DungeonEntity>();
            ent.transform.Find("BackgroundBar(Clone)").GetComponent<FUI_PurityBar>().subUpdate();
        }
    }
}
