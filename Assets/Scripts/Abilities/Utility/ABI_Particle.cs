﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_Particle : MonoBehaviour {

    [HideInInspector]
    public float size;
    [HideInInspector]
    public float lifeTime;
    [HideInInspector]
    public float growTime;

    private float curTime;
    private state curState = state.growing;
    private Vector3 initialScale;



    //
    // All state that particle can bein
    //
    enum state
    {
        growing, idle, shrinking
    }



	//
	// Use this for initialization
	//
	void Awake () {
        //print(transform.localScale);
        initialScale = transform.localScale;
        //print(initialScale);
	}
	


    //
    // Call during growing state
    //
    void grow()
    {
        if (curTime > growTime)
        {
            gameObject.transform.localScale = initialScale * size;
            curState = state.idle;
            idle();
        }
        else
        {
            float scalar = curTime / growTime;
            gameObject.transform.localScale = initialScale * size * scalar;
        }
    }



    //
    // Call during the idle state
    //
    void idle()
    {
        if (curTime >= lifeTime - growTime)
        {
            curState = state.shrinking;
            shrink();
        }
    }



    //
    // Call during shrinking state
    //
    void shrink()
    {
        if (curTime > lifeTime)
        {
            Destroy(gameObject);
        }
        float scalar = 1 - (curTime-(lifeTime - growTime)) / growTime;
        gameObject.transform.localScale = initialScale * size * scalar;
    }



	//
	// Update is called once per frame
	//
	void Update () {
        curTime += Time.deltaTime;
        switch (curState)
        {
            case state.growing:   grow();   break;
            case state.idle:      idle();   break;
            case state.shrinking: shrink(); break;
        }
	}
}
