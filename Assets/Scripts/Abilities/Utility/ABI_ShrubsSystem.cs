﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_ShrubsSystem : MonoBehaviour {

    ParticleSystem ps;
    ParticleSystem.ShapeModule ps_shape;
    CircleCollider2D col;
    public float lifeTime = 10f;
    public float radius = 5f;
    float deltaTime;



    //
    // Use this for initialization
    //
    void Start()
    {
        // get particle system and shape module
        ps = gameObject.GetComponent<ParticleSystem>();
        ps_shape = ps.shape;

        // set radius and lifetime
        ps.startLifetime = lifeTime;
        ps_shape.radius = radius;

        // get collider and set radius
        col = gameObject.GetComponent<CircleCollider2D>();          
        col.radius = radius;
        col.enabled = true;
    }



    //
    // Update is called once per frame
    //
    void Update()
    {
        // check if time to destroy this object
        deltaTime += Time.deltaTime;
        if (deltaTime >= lifeTime + 0.6f)
        {
            Destroy(gameObject);
        }
    }
}
