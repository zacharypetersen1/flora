﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_Projectile : MonoBehaviour
{
    public TME_Time.type timeType = TME_Time.type.game;
    public float Velocity = 0.5f;
    public float maxDistance = 20;
    public string[] collidesWithTags;
    public string[] affectsTags;
    public float DC = 1;
    public float heal = 0;
    public bool purity; //should be equal to the purity of the creator of this projectile
    bool alive = true;
    float distanceTravelled = 0;
    Vector3 direction;
    Vector3 lastPosition;

    public void Start()
    {
        lastPosition = transform.position;
    }

    public void setDirection(Vector3 dir)
    {
        direction = dir;
    }



    //
    // Runs on collision
    //
    void OnTriggerEnter(Collider collider)
    {

        // loop through the collidable tags
        foreach(var tag in collidesWithTags)
        {
            // check if need to destroy particle
            if (collider.tag == tag)
            {
                Destroy(gameObject);
            }
        }
        foreach(string tag in affectsTags)
        {
            if (collider.tag == tag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(DC, heal, CBI_CombatMessenger.ChangePurity.dontChange, null, transform.position));
                    Destroy(gameObject);
                }
                
            }
        }
    }



    //
    // check if max distance reached
    //
    void checkDistance()
    {
       
        distanceTravelled += Vector3.Distance(transform.position, lastPosition);
        if (distanceTravelled >= maxDistance)
        {
            alive = false;
        }
        lastPosition = transform.position;
    }



    //
    // update position
    //
    void updatePosition()
    {
        transform.position += direction * TME_Manager.getDeltaTime(timeType) * Velocity;
        transform.position = UTL_Dungeon.getTerrPosition(transform.position);
    }

    public void FixedUpdate()
    {
        updatePosition();
        checkDistance();

        if (!alive)
        {
            if(gameObject.tag == "basicHoldAttack")
            {
                UTL_GameObject.cloneAtLocation("Abilities/BasicAttack/BasicHoldAttackAOE", gameObject.transform.position);
            }
            Destroy(gameObject);
        }
       
    }
}
