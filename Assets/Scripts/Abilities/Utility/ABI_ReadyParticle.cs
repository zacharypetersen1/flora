﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_ReadyParticle : MonoBehaviour {

    public int rate = 12;
    ParticleSystem system;
    ParticleSystem.EmissionModule em;



	//
	// Use this for initialization
	//
	void Start () {
        system = gameObject.GetComponent<ParticleSystem>();
        em = system.emission;
	}
	


	//
	// Update is called once per frame
	//
	void Update () {
        MAP_NatureData.type t = MAP_NatureMap.natureTypeAtPos(transform.position);
        if (t == MAP_NatureData.type.grass) {
            em.rate = new ParticleSystem.MinMaxCurve(rate);
        }
        else if(t == MAP_NatureData.type.rock)
        {
            em.rate = new ParticleSystem.MinMaxCurve(0);
        }
        else
        {
            throw new UnityException("Unable to detect matching nature type: " + t + " within 'AbilityReady' Particle System");
        }
	}
}
