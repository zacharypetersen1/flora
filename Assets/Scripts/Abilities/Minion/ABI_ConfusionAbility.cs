﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_ConfusionAbility : MonoBehaviour
{

    ENT_Body body;
    string affectsTag = "Minion";
    public float confusionAbilityRadius = 30;

    float timer = 0;
    float PULSE_FREQUENCY = 10;
    bool resetTimer = false;

    bool purity = false;
    public bool Purtiy
    {
        get { return purity; }
        set { purity = value; }
    }

    // Use this for initialization
    void Start()
    {
        body = GetComponentInParent<ENT_Body>();

    }

    // Update is called once per frame
    void Update()
    {
        timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (resetTimer)
        {
            timer = PULSE_FREQUENCY;
            resetTimer = false;
        }
        if (body == null || !body.IsAbilityActive)
        {
            Destroy(gameObject);
  
        }
    }

    //
    // Runs on collision
    //
    void OnTriggerStay(Collider collider)
    {
        if (timer <= 0)
        {
            if (collider.tag == affectsTag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(0, 0, CBI_CombatMessenger.ChangePurity.dontChange, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.confusion }, transform.position));
                }

            }
            resetTimer = true;
        }
    }

}
