﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_SlowAbility : MonoBehaviour {

    ENT_Body body;
    string affectsTag = "Minion";
    bool purity = false;
    public float slowAbilityRadius = 30;

    protected int slowFlowerCount;
    public List<GameObject> slowFlowers;
    public bool Purtiy
    {
        get { return purity; }
        set { purity = value; }
    }
    
    // Use this for initialization
    void Start () {
        body = GetComponentInParent<ENT_Body>();
        slowFlowerCount = (int)(slowAbilityRadius * 3);
        spawnSlowFlowers(slowFlowerCount);
	}

    // Update is called once per frame
    void Update() {

        if (body == null || !body.IsAbilityActive)
        {    
            Destroy(gameObject);
            foreach (GameObject slowFlower in slowFlowers)
            {
                Destroy(slowFlower);
            }
        }
	}

    //
    // Runs on collision
    //
    void OnTriggerStay(Collider collider)
    {
            if (collider.tag == affectsTag)
            {
                ENT_DungeonEntity other = collider.gameObject.GetComponent<ENT_DungeonEntity>();
                if (purity != other.Purity)
                {
                    other.receive_combat_message(new CBI_CombatMessenger(0, 0, CBI_CombatMessenger.ChangePurity.dontChange, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.slow}, transform.position));
                }

            }
    }

    void spawnSlowFlowers(int flowerCount)
    {
        for (int i = 0; i < flowerCount; ++i)
        {
            GameObject slowFlower = UTL_GameObject.cloneAtLocation("Abilities/MinionAttacks/SlowAbility/slowObj", UTL_Dungeon.getRandomPointInCircle(transform.position, slowAbilityRadius));
            slowFlower.transform.position = UTL_Dungeon.getTerrPosition(slowFlower.transform.position);
            slowFlower.transform.position -= new Vector3(0, slowFlower.GetComponent<ENV_SlowObj>().distToGrow, 0);
            slowFlowers.Add(slowFlower);
        }
    }

}
