﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_Interactable : FUI_FloatingUI {

    public override void Awake()
    {
        base.Awake();
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Use this for initialization
    public override void Start()
    {
        fui = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/Instructions"));
        setOffset(fui);
        fui.GetComponent<Renderer>().material = material;
        hide();
    }

    public void Update()
    {
        base.update();
    }
}
