﻿// Ben & Matt

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Crosshair : MonoBehaviour {
    // Camera
    Camera worldCamera = new Camera();
    Vector3 middleScreen;
    CAM_ScreenResize camResize;

    // Ray
    Ray ray;
    RaycastHit hit;
    public float maxDistance = 100;
    LayerMask layermask;

    // Target
    GameObject crosshair;
    Renderer crosshairRend;
    Texture[] textures;
    string _target = "invalid";
    Vector3 _targetPos;
    GameObject _targetObject;

    public string target
    {
        get { return _target; }
    }

    public Vector3 targetPos
    {
        get { return _targetPos; }
    }

    public GameObject targetObject
    {
        get { return _targetObject; }
    }

    // Use this for initialization
    void Start () {
        layermask = (1 << 11) | (1 << 12) | (1 << 13);
        worldCamera = GameObject.Find("Camera").GetComponent<Camera>();
        middleScreen = new Vector3(worldCamera.pixelWidth / 2, worldCamera.pixelHeight / 2, worldCamera.nearClipPlane);
        camResize = GameObject.Find("Scripts").GetComponent<CAM_ScreenResize>();

        textures = new Texture[4];
        textures[0] = Resources.Load("UI/Textures/crosshair_dot1") as Texture;
        textures[1] = Resources.Load("UI/Textures/crosshair_dot2") as Texture;
        textures[2] = Resources.Load("UI/Textures/crosshair_X1") as Texture;
        textures[3] = Resources.Load("UI/Textures/crosshair_X2") as Texture;

        crosshair = GameObject.Find("Crosshair");
        crosshairRend = crosshair.GetComponent<Renderer>();
        _targetPos = new Vector3();
        _targetObject = new GameObject();
    }
	
	// Update is called once per frame
	void Update () {
        middleScreen = camResize.midScreen;
        checkTarget();
    }

    void checkTarget()
    {
        ray = worldCamera.ScreenPointToRay(middleScreen);
        if (Physics.Raycast(ray, out hit, maxDistance, layermask))
        {
            if (hit.transform.tag == "Minion")
            {
                ENT_Body body = hit.transform.gameObject.GetComponent<ENT_Body>();
                if (!body.isAlly)
                {
                    _target = "enemy";
                    _targetObject = hit.transform.gameObject;
                    crosshairRend.material.mainTexture = textures[2];
                    crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                }
                else if (body.entity_type == ENT_DungeonEntity.monsterTypes.slowMinion
                    || body.entity_type == ENT_DungeonEntity.monsterTypes.turretMinion
                    || body.entity_type == ENT_DungeonEntity.monsterTypes.necroMinion
                    || body.entity_type == ENT_DungeonEntity.monsterTypes.confusionMinion)
                {
                    _target = "special";
                    _targetObject = hit.transform.gameObject;
                    crosshairRend.material.mainTexture = textures[3];
                    crosshair.transform.localScale = new Vector3(0.003f, 0.003f, 0.003f);
                }
            }
            else if (hit.transform.tag == "ground")
            {
                _target = "neutral";
                _targetPos = hit.point;
                crosshairRend.material.mainTexture = textures[1];
                crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
            }
        }
        else
        {
            _target = "";
            crosshairRend.material.mainTexture = textures[0];
            crosshair.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }
    }
}
