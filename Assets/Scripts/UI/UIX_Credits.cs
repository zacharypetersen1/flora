﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_Credits : MonoBehaviour {

    public float delay = 30;
    public string levelString = "Main_Menu_Scene";

	// Use this for initialization
	void Start () {
        StartCoroutine(delayedLoadLevel(delay));
	}
	
    IEnumerator delayedLoadLevel(float delay)
    {
        yield return new WaitForSeconds(delay);
        Application.LoadLevel(levelString);
    }

}
