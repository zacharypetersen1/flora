﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class UTL_Dungeon {

    public static readonly int max_x = 550;
    public static readonly int max_y = 550;
    public static readonly int min_x = 0;
    public static readonly int min_y = 0;

    //
    // checks if objects are within line of sight
    // Harold
    //
    public static bool checkLOS(Vector3 vA, Vector3 vB, string[] obstructionTags)
    {
        float distance = Vector3.Distance(vA, vB);
        Vector3 direction = (vB - vA).normalized;
        RaycastHit[] hits = Physics.RaycastAll(vA, direction, distance);
        for (int i = 0; i < hits.Length; ++i) {
            RaycastHit hit = hits[i];
            string tag = hit.collider.gameObject.tag;
            if(obstructionTags.Contains(tag))
            {
                return false;
            }
        }
        return true;
    }



    //
    // wrapper for line of sight that checks if any walls are obstructing
    //
    public static bool checkLOSWalls(Vector3 posA, Vector3 posB)
    {
        return checkLOS(posA, posB, new string[] { "dirtWall", "rockWall" });
    }



    //
    // checks if objects are within line of sight with a ray cast thickness
    //
    public static bool checkLOSThickness(Vector3 vA, Vector3 vB, string[] obstructionTags, float radius)
    {
        float distance = Vector3.Distance(vA, vB);
        Vector3 direction = (vB - vA).normalized;
        RaycastHit[] hits = Physics.SphereCastAll(vA, radius, direction, distance);
        for (int i = 0; i < hits.Length; ++i)
        {
            RaycastHit hit = hits[i];
            string tag = hit.collider.gameObject.tag;
            if (obstructionTags.Contains(tag))
            {
                return false;
            }
        }
        return true;
    }



    //
    // wrapper for line of sight that checks if any walls are obstructing with thick ray cast
    //
    public static bool checkLOSWallsThickness(Vector3 posA, Vector3 posB, float radius)
    {
        return checkLOSThickness(posA, posB, new string[] { "dirtWall", "rockWall" }, radius);
    }



    //
    //
    //
    /*
    public statuc void checkBoundsWalls(Vector2 position)
    {
        return Collider2D
    }
    */



    //
    // Converts to correct position offset by terrain height
    //
    public static Vector3 getTerrPosition(Vector3 position)
    {
        /*GameObject[] groundObjects = GameObject.FindGameObjectsWithTag("ground");
        foreach (var groundObj in groundObjects)
        {*/
        //Debug.Log("ran test");
        /*RaycastHit hit;
        Ray ray = new Ray(new Vector3(position.x, position.y + 5, position.z), new Vector3(0, -1, 0));
        if(Physics.Raycast(ray, out hit, 2.0f * 50))
        {
            return hit.point;
        }
        else
        {

        }
        //}
        return position;*/
        RaycastHit[] hits;
        Vector3 defaultTallestPoint = new Vector3(-100F, -100F, -100F);
        Vector3 tallestGroundPoint = defaultTallestPoint; //arbitrary very negative vector
        //Ray ray = new Ray(new Vector3(position.x, position.y + 5, position.z), new Vector3(0, -1, 0));
        hits = Physics.RaycastAll(new Vector3(position.x, position.y + 5, position.z), Vector3.down, 100f);
        foreach (RaycastHit hit in hits)
        {
            if (hit.transform.gameObject.tag == "ground")
                if (Vector3.Distance(position, new Vector3(position.x, hit.transform.position.y, position.z))
                    < Vector3.Distance(position, new Vector3(position.x, tallestGroundPoint.y, position.z))) //only factor in y value
                    tallestGroundPoint = hit.point;
        }
        if (!(tallestGroundPoint == defaultTallestPoint)) return tallestGroundPoint;
        else return position + Vector3.down * Mathf.Infinity;
        
    }

    //probably doesn't work as intended
    public static bool pointWithinTerrain(Vector3 point)
    {
        RaycastHit hit;
        Ray ray = new Ray(new Vector3(point.x, point.y, -50), new Vector3(0, 0, 1));
        if (Physics.Raycast(ray, out hit, 2.0f * 50))
        {
            return true;
        }
        else return false;
    }

    public static GameObject getNearestWithTag(Vector3 position, string tag)
    {
        GameObject[] things = GameObject.FindGameObjectsWithTag(tag);
        if (things.Length == 0) return null;
        float dist = 1000000;
        GameObject nearest = null;
        foreach (GameObject thing in things)
        {
            float tempDist = Vector3.Distance(position, thing.transform.position);
            if (tempDist<= dist)
            {
                dist = tempDist;
                nearest = thing;
            }
        }
        return nearest;
    }

    public static List<GameObject> getNearestListWithTag(Vector3 position, string tag, float distance)
    {
        GameObject[] things = GameObject.FindGameObjectsWithTag(tag);
        if (things.Length == 0) return null;
        List<GameObject> nearest = new List<GameObject>();
        foreach (GameObject thing in things)
        {
            float tempDist = Vector3.Distance(position, thing.transform.position);
            if (tempDist <= distance)
            {
                nearest.Add(thing);
            }
        }
        return nearest;
    }

    private static Dictionary<ENT_DungeonEntity.monsterTypes, string> minionToPath = new Dictionary<ENT_DungeonEntity.monsterTypes, string>()
    {
        { ENT_DungeonEntity.monsterTypes.flowerMinion, "Minions/TierOneFlower"},
        { ENT_DungeonEntity.monsterTypes.treeMinion, "Minions/TierOneTree" },
        { ENT_DungeonEntity.monsterTypes.mushroomMinion, "Minions/TierOneMushroom" },
        { ENT_DungeonEntity.monsterTypes.slowMinion, "Minions/SlowAlly" },
        { ENT_DungeonEntity.monsterTypes.turretMinion, "Minions/TurretAlly"},
        { ENT_DungeonEntity.monsterTypes.necroMinion, "Minions/NecroAlly"}

    };

    public static GameObject spawnMinion(ENT_DungeonEntity.monsterTypes type, Vector3 location, bool isAlly = false, List<Vector3> patrolPoints= null)
    {
        string path = minionToPath[type];
        GameObject minion = UTL_GameObject.cloneAtLocation(path, location);
        ENT_Body body = minion.GetComponent<ENT_Body>();
        body.startsAsAlly = isAlly;
        body.PatrolPoints = patrolPoints;
        return minion;
    }

    public static GameObject spawnMinion(ENT_DungeonEntity.monsterTypes type, float x_loc, float y_loc, float z_loc, bool isAlly = false, List <Vector3> patrolPoints = null)
    {
        Vector3 location = new Vector3(x_loc, y_loc, z_loc);
        return spawnMinion(type, location, isAlly, patrolPoints);
    }

    public static Vector3 getRandomPointInCircle(Vector3 center, float radius) {
        float angle = Random.Range(0F, 355F);
        Vector3 vec = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle), 0, Mathf.Cos(Mathf.Deg2Rad * angle));
        vec = vec * (radius * Random.Range(0F, 1F));
        return center + vec;
    }
}
