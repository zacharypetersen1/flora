﻿// Zach

using UnityEngine;
using System.Collections;


public static class UTL_GameObject {
    
    //
    // clones object at "pathName" from the resources folder at specified location
    //
    /*public static void cloneAtLocation(string pathName, Vector3 location, float z)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pathName));
        temp.transform.position = new Vector3(location.x, location.y, z);
    }*/



    //
    // clones object at "pathName" from the resources folder at specified location
    //
    public static GameObject cloneAtLocation(string pathName, Vector3 location)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pathName));
        temp.transform.position = UTL_Dungeon.getTerrPosition(location);
        return temp;
    }



    //
    // clones object at "pathName" from the resources folder at specified location
    //
    public static GameObject cloneAtLocation(string pathName, Vector2 location)
    {
        return cloneAtLocation(pathName, new Vector3(location.x, location.y, 0));
    }

    //Harold
    //
    // clones object at specified location
    //
    public static GameObject cloneAtLocation(GameObject toClone, Vector3 location)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(toClone);
        temp.transform.position = UTL_Dungeon.getTerrPosition(location);
        return temp;
    }



    //
    // clones object at specified location
    //
    public static GameObject cloneAtLocation(GameObject toClone, Vector2 location)
    {
        return cloneAtLocation(toClone, new Vector3(location.x, location.y, 0));
    }



    //
    // clones object at "pathName" from the resources folder at specified location
    //
    /*public static void cloneAtLocation(string pathName, Vector2 location, float z)
    {
        cloneAtLocation(pathName, new Vector3(location.x, location.y, 0), z);
    }*/
}
