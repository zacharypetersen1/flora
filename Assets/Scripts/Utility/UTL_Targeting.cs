﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UTL_Targeting {

    public static GameObject target;

    //
    // Finds Nearest Target Based on Distance to Player and Camera Angle
    //
    public static GameObject getTarget()
    {
        CAM_Camera cameraScript = GameObject.Find("Camera").GetComponent<CAM_Camera>();
        if (cameraScript.currentState == CAM_State.type.targeting)
        {
            return target;
        }
        GameObject player = GameObject.Find("Player");
        //Max Distance to Target From Player
        float MaxD = 25f;
        //Max Angle to Target From Camera
        float MaxA = 45f;
        //Direction Camera is Facing
        Vector3 CamForward = cameraScript.transform.forward;
        GameObject obj = null;
        foreach (GameObject enemyObj in GameObject.FindGameObjectsWithTag("Minion"))
        {
            if (enemyObj.GetComponent<ENT_Body>().isAlly)
            {
                continue;
            }
            Vector3 AngleToTarget = enemyObj.transform.position - cameraScript.transform.position;
            float thisD = Vector3.Distance(player.transform.position, enemyObj.transform.position);
            float thisA = Vector3.Angle(AngleToTarget, CamForward);
            if (thisA < MaxA)
            {
                MaxA = thisA;
                if (thisD < MaxD)
                {
                    if (Vector3.Distance(cameraScript.transform.position, player.transform.position) <= Vector3.Distance(cameraScript.transform.position, enemyObj.transform.position))
                    {
                        obj = enemyObj;
                    }
                }
            }
        }
        target = obj;
        //Debug.Log(obj);
        return obj;
    }
}
