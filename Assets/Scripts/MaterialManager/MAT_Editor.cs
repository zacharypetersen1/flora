﻿#if (UNITY_EDITOR) 

using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MAT_Manager))]
public class MAT_Editor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();
        MAT_Manager myScript = (MAT_Manager)target;

        //
        //Buttons to set path to certain resource
        //
        if (GUILayout.Button("Meow"))
        {
            Debug.Log("Cat");
        }
    }
}
#endif