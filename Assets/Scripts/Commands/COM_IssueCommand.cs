﻿// Matt & Ben
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COM_IssueCommand : MonoBehaviour
{
    GameObject commandObject;
    Vector3 commandObjectHome;

    SEL_Manager selectMan;
    List<GameObject> selected;
    COM_AllyCommands playerCommands;
    UIX_Crosshair crosshair;

    private bool singleClick = false;
    private float doubleClickTimeLimit = 0;
    public float delay = 0.2f;

    // Use this for initialization
    void Start()
    {
        commandObject = GameObject.Find("CommandPointObject");
        commandObjectHome = commandObject.transform.position;

        selectMan = GameObject.Find("SelectionManager").GetComponent<SEL_Manager>();
        playerCommands = GameObject.Find("Player").GetComponent<COM_AllyCommands>();

        crosshair = GameObject.Find("Crosshair").GetComponent<UIX_Crosshair>();
    }

    // Update is called once per frame
    void Update()
    {
        if (INP_PlayerInput.getButtonDown("Command"))
        {
            determineClick();
        }
        if ((Time.time - doubleClickTimeLimit) > delay)
        {
            if (singleClick)
            {
                switch (crosshair.target)
                {
                    case "enemy":
                        // Attack
                        Debug.Log("Attack");
                        selected = selectMan.selectedList;
                        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_attack");
                        playerCommands.AttackCommand(selected, crosshair.targetObject); //
                        break;
                    case "neutral":
                        // Goto
                        Debug.Log("Go Here, Pos:" + crosshair.targetPos);
                        selected = selectMan.selectedList;
                        FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_goto");
                        playerCommands.GoToCommand(selected, crosshair.targetPos); //
                        break;
                    default:
                        Debug.Log("Invalid Command");
                        break;
                }
                singleClick = false;
            }
        }
        if (INP_PlayerInput.getButtonDown("AllyAbility") && crosshair.target == "special")
        {
            Debug.Log("Activate Ability");
            selected = selectMan.selectedList;
            FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_ability");
            playerCommands.ActivateCommand(crosshair.targetObject);
        }
    }

    void moveCommandPoint(Vector3 newPos)
    {
        if (crosshair.target == "neutral")
        { 
            commandObject.transform.position = newPos;
        }
        else if(crosshair.target == "enemy")
        {
            commandObject.transform.position = commandObjectHome;
        }
    }

    void commandPointToHome()
    {
        commandObject.transform.position = commandObjectHome;
    }

    void determineClick()
    {
        if (!INP_PlayerInput.getButton("SelectMode") && !INP_PlayerInput.getButton("SelectMode2"))
        {
            if (!singleClick)
            {
                singleClick = true;
                moveCommandPoint(crosshair.targetPos);
                doubleClickTimeLimit = Time.time;
            }
            else if ((Time.time - doubleClickTimeLimit) < delay) // if true, was a double click
            {
                singleClick = false;
                commandPointToHome();
                Debug.Log("Follow");
                selected = selectMan.selectedList;
                FMODUnity.RuntimeManager.PlayOneShot("event:/twig_command_follow");
                playerCommands.FollowCommand(selected);
            }
        }
    }

}

