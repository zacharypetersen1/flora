﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Harold
//should be attached to player gameObject

public class COM_AllyCommands : MonoBehaviour {

    protected PLY_AllyManager am;
	// Use this for initialization
	void Start () {
        am = GetComponent<PLY_AllyManager>();
	}
    #region list of minion commands
    public void FollowCommand(List<GameObject> allies)
    {
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            FollowCommand(ally);
            am.followGroup.addToList(ally);
            body.commandGroup = am.followGroup;
        }
    }

    public void WaitCommand(List<GameObject> allies)
    {
        AIC_AllyGroup waitGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.wait, allies, am, am.transform.position);
        am.commandGroups.Add(waitGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            WaitCommand(ally);
            body.commandGroup = waitGroup;
        }
    }

    public void GoToCommand(List<GameObject> allies, Vector3 goToPos)
    {
        AIC_AllyGroup goToGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.goTo, allies, am, goToPos);
        am.commandGroups.Add(goToGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            GoToCommand(ally, goToPos);
            body.commandGroup = goToGroup;
        }
    }

    public void AttackCommand(List<GameObject> allies, GameObject target)
    {
        AIC_AllyGroup attackGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.attack, allies, am, target.transform.position, target);
        am.commandGroups.Add(attackGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            AttackCommand(ally, target);
            body.commandGroup = attackGroup;
        }
    }

    public void ActivateCommand(List<GameObject> allies)
    {
        AIC_AllyGroup activateGroup = new AIC_AllyGroup(PLY_AllyManager.allyState.activate, allies, am, Vector3.zero);
        am.commandGroups.Add(activateGroup);
        foreach (GameObject ally in allies)
        {
            ENT_Body body = ally.GetComponent<ENT_Body>();
            if (body.commandGroup != null) body.commandGroup.removeFromList(ally);
            ActivateCommand(ally);
            body.commandGroup = activateGroup;
        }
    }
    #endregion
    #region single minion commands
    public void ActivateCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Activate command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Activate command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.activate);
    }

    public void FollowCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Follow command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Follow command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.follow);
    }

    public void WaitCommand(GameObject ally)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Wait command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Wait command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.wait);
    }

    public void AttackCommand(GameObject ally, GameObject target)
    {
        if (target == null)
        {
            Debug.LogWarning("target to attack cannot be null");
            return;
        }
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.attack, target);
    }

    public void GoToCommand(GameObject ally, Vector3 goToPos)
    {
        if (goToPos == null)
        {
            Debug.LogWarning("position to go to cannot be null. you may want to call WaitCommand instead");
            return;
        }
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it does not have a body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("cannot run Attack command on " + ally.name + " because it is not an ally");
            return;
        }
        am.setState(ally, PLY_AllyManager.allyState.goTo, goToPos);
    }
    #endregion
}
