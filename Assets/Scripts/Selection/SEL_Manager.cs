﻿// Andrei & Sterling
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class SEL_Manager : MonoBehaviour {
    SelectionManager selectionManager;
    public List<GameObject> selectedList;

    // Use this for initialization
    void Start () {
        selectionManager = GetComponent<SelectionManager>();
        selectedList = new List<GameObject>();
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
        selectedList = GetComponent<SelectionManager>().GetSelectedObjectsAsList();
        if (INP_PlayerInput.getButton("SelectMode") || INP_PlayerInput.getButton("SelectMode2"))
        {
            CAM_Dolly.locked = true;
            selectionManager.selectMode = true;
            //printHovered();
        }
        else
        {
            CAM_Dolly.locked = false;
            selectionManager.selectMode = false;
            //printSelected();
        }
    }

    void printSelected()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            var type = child.GetComponent<ENT_Body>().entity_type;
            if (totalCount.ContainsKey(type))
            {
                totalCount[type] += 1;
            }
            else totalCount.Add(type, 1);
        }

        // Count selected minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in selectedList)
        {
            var type = obj.GetComponent<ENT_Body>().entity_type;
            if (selectedCount.ContainsKey(type))
            {
                selectedCount[type] += 1;
            }
            else selectedCount.Add(type, 1);
        }

        // Print
        StringBuilder stringBuilder = new StringBuilder();
        foreach (KeyValuePair<ENT_DungeonEntity.monsterTypes, int> pair in selectedCount)
        {
            stringBuilder.Append("[").Append(pair.Key).Append(": ").Append(pair.Value).Append(" / ").Append(totalCount[pair.Key]).Append("]");
        }
        Debug.Log(stringBuilder.ToString());
    }

    void printHovered()
    {
        // Count total minions
        Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (Transform child in GameObject.Find("UnitContainer").transform)
        {
            var type = child.GetComponent<ENT_Body>().entity_type;
            if (totalCount.ContainsKey(type))
            {
                totalCount[type] += 1;
            }
            else totalCount.Add(type, 1);
        }
        //Dictionary<ENT_DungeonEntity.monsterTypes, int> totalCount = countMinions<UnityEngine.Transform>(GameObject.Find("UnitContainer").transform);

        // Count selected minions
        Dictionary <ENT_DungeonEntity.monsterTypes, int> selectedCount = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
        foreach (GameObject obj in GetComponent<SelectionManager>().GetHoveredObjects())
        {
            var type = obj.GetComponent<ENT_Body>().entity_type;
            if (selectedCount.ContainsKey(type))
            {
                selectedCount[type] += 1;
            }
            else selectedCount.Add(type, 1);
        }

        // Print
        StringBuilder stringBuilder = new StringBuilder();
        foreach (KeyValuePair<ENT_DungeonEntity.monsterTypes, int> pair in selectedCount)
        {
            stringBuilder.Append("[").Append(pair.Key).Append(": ").Append(pair.Value).Append(" / ").Append(totalCount[pair.Key]).Append("]");
        }
        Debug.Log(stringBuilder.ToString());
    }

    //Dictionary<ENT_DungeonEntity.monsterTypes, int> countMinions<T>(List<T> minions)
    //{
    //    Dictionary<ENT_DungeonEntity.monsterTypes, int> count = new Dictionary<ENT_DungeonEntity.monsterTypes, int>();
    //    foreach (T obj in minions)
    //    {
    //        var type = obj.GetComponent<ENT_Body>().entity_type;
    //        if (count.ContainsKey(type))
    //        {
    //            count[type] += 1;
    //        }
    //        else count.Add(type, 1);
    //    }
    //    return count;
    //}
}
