﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_Anim : MonoBehaviour {



    public Animator animator;
    public CHA_Motor motor;
    public float lookAtEndAngle = 55;
    public float lookAtFalloff = 30;

    public string footstep = "event:/twig_footstep_grass";

    void OnAnimatorIK(int layerIndex)
    {
        /*float deltaAngle = UTL_Math.getDeltaRotDeg(CAM_Camera.getDollyPhysicalRotZ(), motor.rotation * Mathf.Rad2Deg);
        if (deltaAngle <= lookAtEndAngle)
        {
            animator.SetLookAtWeight(1, 0.5f, 1f);
        }
        else if(deltaAngle <= lookAtEndAngle + lookAtFalloff)
        {
            animator.SetLookAtWeight(1, 0.5f * (1 - (deltaAngle - lookAtEndAngle) / lookAtFalloff), 1f);
        }
        else
        {
            animator.SetLookAtWeight(1, 0f, 1f);
        }

        Vector3 dirVec = UTL_Math.vec2ToVec3(UTL_Math.angleRadToUnitVec(-CAM_Camera.getDollyPhysicalRotZ() * Mathf.Deg2Rad), 0);
        Vector3 lookAtVec = transform.position + 5 * dirVec;
        //animator.SetBoneLocalRotation(HumanBodyBones.Head, new Quaternion(0, CAM_Camera.getDollyPhysicalRotZ(), 0, 0));
        animator.SetLookAtPosition(lookAtVec);*/
        //animator.set
    }

    void footstepTwig()
    {
        // Play footstep sound depending on terrain type
        if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.grass)
        {
            footstep = "event:/twig_footstep_grass";
        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.rock)
        {
            footstep = "event:/twig_footstep_stone";
        }
        else if (MAP_NatureMap.natureTypeAtPos(transform.position) == MAP_NatureData.type.corrupt)
        {
            footstep = "event:/twig_footstep_corrupt";
        }
        FMODUnity.RuntimeManager.PlayOneShot(footstep, transform.position);
    }
}
