﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_StateSurf : PLY_MoveState
{

	// FMOD parameters
	float surfVal = 1;
	float mag;

    //
    // runs when entering state
    //
    public override void StateEnter()
    {
        base.StateEnter();
    }



    //
    // runs once per frame
    //
    public override void StateUpdate()
    {
        // Nothing
    }



    //
    // runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        // Get input
        Vector2 moveAxis = INP_PlayerInput.get2DAxis("Movement", true);

        // Set rotation velocity based on speed
        motor.rotationVelocity = UTL_Math.mapToNewRange(moveAxis.y, -1, 1, driver.surfingRotVelMin, driver.surfingRotVelMax);

        // Alter input to fit surfing mechanics
        moveAxis.x = 0;
        moveAxis.y = UTL_Math.mapToNewRange(moveAxis.y, -1, 1, 0.5f, 1);
        motor.move(CAM_Camera.applyDollyRotZ(moveAxis));

		mag = (motor.getVelocityMagnitude() - 0.5f) * 4f;
		surfVal = Mathf.Clamp (mag, 0, 2);

		//Debug.Log (surfVal);
		MUS_System.singleton.surfSoundSpeed (surfVal);
    }
}
