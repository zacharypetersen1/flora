﻿// Andrei

using System;
using UnityEngine;

public class PLY_State_Vines : PLY_MoveState
{

    float rumbleIntensity = 0.25f;
    CHA_Motor motor;
    PLY_PlayerDriver driver;



    //
    // Runs when entering the state
    //
    public override void StateEnter()
    {
        // get motor and driver
        motor = gameObject.GetComponent<CHA_Motor>();
        driver = gameObject.GetComponent<PLY_PlayerDriver>();

        motor.speed = 0;
        // vvv wot is that vvv
        gameObject.GetComponent<CircleCollider2D>().isTrigger = true;

        //RBL_RumbleManager.turnRumbleOn("treeRoots", rumbleIntensity);
    }



    //
    // Runs once per frame
    //
    public override void StateUpdate()
    {

    }



    //
    // Runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        //motor.move(movementDir);
    }



    //
    // Runs when detected collision occuring
    //
    void OnTriggerEnter2D(Collider2D col)
    {
    }



    //
    // Runs when detected collision over
    //
    void OnTriggerExit2D(Collider2D col)
    {
    }
}