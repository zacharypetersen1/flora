﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_AllyManager : MonoBehaviour {
    public float farFollowDistance = 20;
    public float nearFollowDistance = 12.5f;
    public float threatenedRange = 8; //controls range at which monsters will attack enemies
    private allyState state;
    public int allyCount = 0;
    public const float FOLLOWDISTANCESCALE = 0.5F; //0.5
    //[HideInInspector]
    //public Vector3 compareToPos;

    //public List<float> allyFollowDistances; 

    public enum allyState
    {
        follow, wait, goTo, activate, attack
    }

    public List<AIC_AllyGroup> commandGroups = new List<AIC_AllyGroup>();
    public AIC_AllyGroup followGroup;

    //private Vector3 waitPosition = Vector3.zero;

    private Transform twigTransform;
   
    private List<GameObject> ally_group_1 = new List<GameObject>();
    private List<GameObject> ally_group_2 = new List<GameObject>();
    private List<GameObject> ally_group_3 = new List<GameObject>();
    private List<GameObject> ally_group_4 = new List<GameObject>();
    private List<GameObject> ally_group_5 = new List<GameObject>();
    private List<GameObject> ally_group_6 = new List<GameObject>();
    private List<GameObject> ally_group_7 = new List<GameObject>();
    private List<List<GameObject>> allies = new List<List<GameObject>>();
    private List<GameObject> allAllies = new List<GameObject>();
    private List<GameObject> allStructures = new List<GameObject>();

    private List<GameObject> enemies = new List<GameObject>();

    public List<List<GameObject>> getAllies() { return allies; }
    public List<GameObject> getEnemies() { return enemies; }

    public List<GameObject> getAllEnemies
    {
        get { return enemies; }
    }

    public List<GameObject> getAllAllies
    {
        get { return allAllies; }
    }

    public List<GameObject> getAllStructures
    {
        get { return allStructures; }
    }

    private void Awake()
    {
        allies.Add(ally_group_1);
        allies.Add(ally_group_2);
        allies.Add(ally_group_3);
        allies.Add(ally_group_4);
        allies.Add(ally_group_5);
        allies.Add(ally_group_6);
        allies.Add(ally_group_7);

        followGroup = new AIC_AllyGroup(allyState.follow, new List<GameObject>(), this, transform.position);
        commandGroups.Add(followGroup);
    }

    #region setStateFunctions
    public void setState(GameObject ally, allyState newState)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, Vector3 goToPos)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                //need to set gotoposition
                body.WaitPos = goToPos;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                break;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                Debug.LogWarning("Error: must pass in a target to attack when changing state to Attack. Try passing in the enemy to attack.");
                return;
        }
    }

    public void setState(GameObject ally, allyState newState, GameObject attackTarget)
    {
        ENT_Body body = ally.GetComponent<ENT_Body>();
        if (body == null)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it does not have an ENT_Body component");
            return;
        }
        if (!body.Purity)
        {
            Debug.LogWarning("Error: cannot setState of " + ally.name + "because it is an enemy");
            return;
        }
        switch (newState)
        {
            case allyState.follow:
                body.CurrentTree = ENT_Body.BehaviorTree.AllyFollowTree;
                break;
            case allyState.wait:
                body.WaitPos = body.transform.position;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyWaitTree;
                break;
            case allyState.goTo:
                Debug.LogWarning("Error: must pass in a gotoPosition in order to change state to go to. Try passing in a Vector3 position to go to.");
                //body.CurrentTree = ENT_Body.BehaviorTree.AllyGoToTree;
                return;
            case allyState.activate:
                if (ally.GetComponent<ENT_MinionAbility>() == null)
                {
                    return;
                }
                body.CurrentTree = ENT_Body.BehaviorTree.AllyActivateTree;
                break;
            case allyState.attack:
                body.AttackTarget = attackTarget;
                body.CurrentTree = ENT_Body.BehaviorTree.AllyAttackTree;
                break;
        }
    }

    public void setAllAlliesState(allyState newState)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState);
            }
        }
    }

    public void setAllAlliesState(allyState newState, Vector3 goToPos)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState, goToPos);
            }
        }
    }

    public void setAllAlliesState(allyState newState, GameObject target)
    {
        //if (state == newState) return;
        state = newState;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                setState(ally, newState, target);
            }
        }
    }
    #endregion

    public allyState getState()
    {
        return state;
    }


    public void addStructure(GameObject newStructure)
    {
        allStructures.Add(newStructure);
    }

    public void removeStructure(GameObject exStructure)
    {
        allStructures.Remove(exStructure);
    }

    public AIC_AllyGroup addAlly(GameObject newAlly)
    {
        ENT_Body body = newAlly.GetComponent<ENT_Body>();
        if (body == null) return null;
        if (!body.Purity)
        {
            print("Error: Tried to add an evil monster " + newAlly.name + "to ally list");
            return null;
        }
        allAllies.Add(newAlly);
        allies[(int)body.getMyType()].Add(newAlly);
        //allyFollowDistances.Add(allyCount);
        allyCount++;
        //setAllAlliesState(getState());

        followGroup.addToList(newAlly);
        return followGroup;
        //print("addAlly called with object: " + newAlly.name);
    }

    public void addEnemy(GameObject newEnemy)
    {
        ENT_Body body = newEnemy.GetComponent<ENT_Body>();
        if (body == null) return;
        if (body.Purity)
        {
            print("Error: Tried to add a good creature " + newEnemy.name + "to enemy list");
            return;
        }
        enemies.Add(newEnemy);
        body.CurrentTree = ENT_Body.BehaviorTree.WorkTree;
    }

    public void removeAlly(GameObject exAlly)
    {
        foreach(List<GameObject> group in allies)
        {
            if (group.Contains(exAlly))
            {
                allAllies.Remove(exAlly);
                group.Remove(exAlly);
                //allyFollowDistances.RemoveAt(allyCount); //remove element at end of array
                allyCount--;
            }

        }        
        //else print("Warning: Tried to remove " + exAlly.name + " From Ally list but it is not in Ally list");
    }

    public void removeEnemy(GameObject exEnemy)
    {
        if (enemies.Contains(exEnemy)) enemies.Remove(exEnemy);
        //else print("Warning: Tried to remove " + exEnemy.name+ " From enemy list but it is not in enemy list");
    }

    //return null if no enemies in enemy list
    public GameObject getNearestEnemy(Vector3 pos, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = 1000000;
        foreach (GameObject enemy in enemies)
        {
            float dist = Vector3.Distance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                nearest = enemy;
                leastDist = dist;
            }
        }
        if (includeStructures)
        {
            foreach (GameObject structure in allStructures)
            {
                float dist = Vector3.Distance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        //Debug.Log(nearest.name);
        return nearest;
    }

    public GameObject getNearestEnemyToTwig()
    {
        return getNearestEnemy(transform.position);
    }

    public GameObject getNearestVisibleEnemy(Vector3 pos, float maxRange, bool includeStructures = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange;
        foreach (GameObject enemy in enemies)
        {
            if (!UTL_Dungeon.checkLOSWalls(pos, enemy.transform.position)) continue;
            float dist = Vector3.Distance(pos, enemy.transform.position);
            if (dist < leastDist)
            {
                nearest = enemy;
                leastDist = dist;
            }
        }
        if (includeStructures)
        {
            foreach (GameObject structure in allStructures)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, structure.transform.position)) continue;
                float dist = Vector3.Distance(pos, structure.transform.position);
                if (dist < leastDist)
                {
                    nearest = structure;
                    leastDist = dist;
                }
            }
        }
        return nearest;
    }

    public GameObject getNearestAlly(Vector3 pos, bool includeTwig = true)
    {
        GameObject nearest = null;
        float leastDist = 1000000;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                float dist = Vector3.Distance(pos, ally.transform.position);
                if (dist < leastDist)
                {
                    nearest = ally;
                    leastDist = dist;
                }
            }
        }
        if (includeTwig)
        {
            float dist = Vector3.Distance(pos, gameObject.transform.position);
            if (dist < leastDist)
            {
                nearest = gameObject;
                leastDist = dist;
            }
        }
        return nearest;
    }

    public GameObject getNearestVisibleAlly(Vector3 pos, float maxRange, bool includeTwig = true)
    {
        GameObject nearest = null;
        float leastDist = maxRange;
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                if (!UTL_Dungeon.checkLOSWalls(pos, ally.transform.position)) continue;
                float dist = Vector3.Distance(pos, ally.transform.position);
                if (dist < leastDist)
                {
                    nearest = ally;
                    leastDist = dist;
                }
            }
        }
        if (includeTwig)
        {
            if (UTL_Dungeon.checkLOSWalls(pos, gameObject.transform.position)) {
                float dist = Vector3.Distance(pos, gameObject.transform.position);
                if (dist < leastDist)
                {
                    nearest = gameObject;
                    leastDist = dist;
                }
            }
        }
        return nearest;
    }

    public Vector3 getMidpoint()
    {
        Vector3 tempVec = Vector3.zero;
        int ally_count = 0;
        foreach (List<GameObject> group in allies)
        {
            foreach(GameObject ally in group)
            {
                ++ally_count;
                tempVec += ally.transform.position;
            }
            
        }
        tempVec /= ally_count;
        return tempVec;
    }

    public bool isMidpointNearTwig()
    {
        Vector3 midpoint = getMidpoint();
        return Vector3.Distance(twigTransform.position, midpoint) < farFollowDistance;
    }

    public bool amINearTwig(Vector3 myPos)
    {
        return Vector3.Distance(twigTransform.position, myPos) < (nearFollowDistance);
    }

    /*public Vector3 getWaitPosition()
    {
        //print("waitPosition: " + waitPosition);
        return waitPosition;
    }*/

    public void setWaitPosition()
    {
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                ally.GetComponent<ENT_Body>().WaitPos = twigTransform.position;
            }
        }
    }

    public void setWaitPosition(Vector2 newWaitPos)
    {
        foreach (List<GameObject> group in allies)
        {
            foreach (GameObject ally in group)
            {
                ally.GetComponent<ENT_Body>().WaitPos = newWaitPos;
            }
        }
    }
    /*

    public bool amINearWaitPos(Vector3 myPos)
    {
        return Vector3.Distance(waitPosition, myPos) < individualFollowDistance;
    }

    public bool isMidpointNearWaitPos()
    {
        Vector3 midpoint = getMidpoint();
        return Vector3.Distance(waitPosition, midpoint) < midpointFollowDistance;
    }*/


    // Use this for initialization
    void Start () {
        twigTransform = gameObject.transform;
        setWaitPosition();
	}
	
	// Update is called once per frame
	void Update () {
        //setFollowDistances();
        foreach (AIC_AllyGroup group in commandGroups)
        {
            group.setFollowDistances();
        }
	}

    public List<GameObject> getAlliesOfType(ENT_DungeonEntity.monsterTypes type)
    {
        return allies[(int)type];
    }

    //public 
}
