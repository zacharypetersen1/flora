﻿// Zach

using System;
using UnityEngine;

public class PLY_State_Normal : PLY_MoveState
{



    //
    // runs when entering state
    //
    public override void StateEnter()
    {
        base.StateEnter();
    }



    //
    // runs once per frame
    //
    public override void StateUpdate()
    {
        // Nothing
    }



    //
    // runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        motor.move(CAM_Camera.applyDollyRotZ(INP_PlayerInput.get2DAxis("Movement", true)));
    }
}
