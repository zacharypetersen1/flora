﻿//Devon

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COL_BuildingCam : MonoBehaviour {


    //
    // When entering a building changes camera to locked state and changes target camera location
    //
    void OnTriggerEnter2D(Collider2D collider)
    {
		if (collider.tag == "BuildingEntry")
        {
            CAM_Camera.setState(CAM_State.type.locked);
            CAM_Camera.singleton.setActiveTarget(collider.transform.Find("BuildingCam").position, collider.transform.Find("BuildingCam").rotation);
        }
	}


    //
    // When exiting a building changes camera to returning state and changes target to camera dolly
    //
    void OnTriggerExit2D(Collider2D collider)
    {
        if (CAM_Camera.singleton.currentState == CAM_State.type.locked)
        {
            if (collider.tag == "BuildingExit")
            {
                CAM_Camera.setState(CAM_State.type.returning);
            }
        }
    }
}