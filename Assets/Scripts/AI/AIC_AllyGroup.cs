﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIC_AllyGroup
{
    PLY_AllyManager.allyState state;
    List<GameObject> allyList;
    Vector3 position;
    PLY_AllyManager am;
    GameObject target;
    float farFollowDistance;

    public AIC_AllyGroup(PLY_AllyManager.allyState newState, List<GameObject> myList, PLY_AllyManager allGroups, Vector3 pos, GameObject attackTarget = null)
    {
        state = newState;
        allyList = myList;
        am = allGroups;
        position = pos;
        target = attackTarget;
        
    }

    public void sortList()
    {
        if (allyList.Count <= 0) return;
        switch (state)
        {
            case PLY_AllyManager.allyState.follow:
                position = am.transform.position;
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.wait:
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.goTo:
                allyList.Sort(compareByDist);
                break;
            case PLY_AllyManager.allyState.activate:
                //am.compareToPos = position;
                //allyList.Sort(am.compareByDist);
                break;
            case PLY_AllyManager.allyState.attack:
                //something has to update pos here
                position = target.transform.position;
                allyList.Sort(compareByDist);
                break;

        }
    }

    public GameObject this[int key]
    {
        get
        {
            return allyList[key];
        }
    }

    public int getCount()
    {
        return allyList.Count;
    }

    public void addToList(GameObject ally)
    {
        allyList.Add(ally);
    }

    public void removeFromList(GameObject ally)
    {
        if (allyList.Contains(ally))
        {
            bool res = allyList.Remove(ally);
            if (!res) Debug.Log("Couldn't remove " + ally.name + " from list");
        }
        if (state != PLY_AllyManager.allyState.follow && allyList.Count <= 0)
        {
            bool res = am.commandGroups.Remove(this);
            if (!res) Debug.Log("Couldn't remove " + allyList + " from command Groups");
        }
    }


    public int compareByDist(GameObject a, GameObject b)
    {
        if (a == null || b == null) return 0;
        float distA = Vector3.Distance(a.transform.position, position);
        float distB = Vector3.Distance(b.transform.position, position);
        if (distA < distB) return -1;
        if (distA > distB) return 1;
        return 0;
    }


    public float getFollowDistance(int index)
    {
        if (state == PLY_AllyManager.allyState.follow) return (am.nearFollowDistance / 2) + (PLY_AllyManager.FOLLOWDISTANCESCALE * index);
        else return PLY_AllyManager.FOLLOWDISTANCESCALE * index;
    }

    public void setFollowDistances()
    {
        sortList();
        setFarFollow();
        for (int i = 0; i < getCount(); ++i)
        {
            GameObject ally = allyList[i];
            ENT_Body body = ally.GetComponent<ENT_Body>();
            body.myFollowDistance = getFollowDistance(i);
            body.farFollowRadius = farFollowDistance;
        }
        
    }

    public void setFarFollow()
    {
        farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        if (state == PLY_AllyManager.allyState.follow) farFollowDistance = Mathf.Max(50, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
        else farFollowDistance = Mathf.Max(25, Mathf.Min(getFollowDistance(allyList.Count - 1) * 2, 100));
    }
}