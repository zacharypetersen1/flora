﻿// Zach

using UnityEngine;
using System;
using System.Collections;

public class CHA_Motor : MonoBehaviour {

    public float acceleration = 0.02f;
    public float gravity = -2f;
    public float jumpForce = 1.5f;
    public float speed = 2f;
    private Vector2 targetVelocity = Vector2.zero;
    //public Vector2 velocity = Vector2.zero;
    public float rotation = 0;                     // character's current rotation
    public bool clampVelocityToDirection = false;   // if true, character only moves in the direction they are facing
    public float rotationVelocity = 1f;             // Scalar for rotation constant
    private float rotationConstant = Mathf.PI/20f;  // constant rotation velocity for linear rotation
    public bool sendAnimData = false;
    public bool sendSoundData = false;
    public GameObject animObject;
    private Animator animator;
    private Vector3 pastLocation;
    public TME_Time.type updateTime = TME_Time.type.game;   // motor will update on this time
    private SFX_System soundSystem;
    public GameObject soundTargetObject;
    private float addJumpForce = 0;
    private Vector2 forceVel = Vector2.zero;
    public float forceAccel = 0.03f;



    //
    // initialize
    //
    void Start()
    {
        pastLocation = transform.position;
        
        // Make sure that the palyer does not send sound data w/o animation data
        if(sendSoundData && !sendAnimData)
        {
            throw new UnityException("Cannot send sound information without sending animation information");
        }

        // Get references to locations where sound/anim data will be sent
        if (sendAnimData)
        {
            animator = animObject.GetComponent<Animator>();
            pastLocation = transform.position;
        }
        if (sendSoundData)
        {
            soundSystem = soundTargetObject.GetComponent<SFX_System>();
        }
    }



    //
    // returns CH_Motor attatched to specified game object
    //
    public static CHA_Motor get(string gameObjectName)
    {
        try
        {
            CHA_Motor temp = GameObject.Find(gameObjectName).GetComponent<CHA_Motor>();
            return temp;
        }
        catch (NullReferenceException e)
        {
            throw new UnityException("Unable to find CH_Motor for object named: '" + gameObjectName + "'");
        }
    }


    //
    // sets z rotation of character
    //
    public void setRotation(Vector2 input)
    {
        float targetRotation = UTL_Math.vecToAngleRad(input);
        float scaledRotationVelocity = rotationConstant * rotationVelocity * input.magnitude;

        // check if angle is within one step of target angle, be sure to catch case that wraps past 2 PI
        if(    Mathf.Abs(targetRotation              - rotation) <= scaledRotationVelocity 
            || Mathf.Abs(targetRotation + 2*Mathf.PI - rotation) <= scaledRotationVelocity)
        {
            rotation = targetRotation;
        }

        // determine if rotation is positive or negative
        else
        {
            // apply rotation so that "rotation" variable would be 0 and targetRotation is at same relative angle
            targetRotation = (targetRotation - rotation + 2 * Mathf.PI) % (2 * Mathf.PI);

            // determine correct direction of rotation
            if (targetRotation <= Mathf.PI)
            {
                
                // calculate new rotation accounting for clock delta time
                rotation += scaledRotationVelocity * TME_Manager.getDeltaTime(updateTime) * 60;
                if(rotation >= 2 * Mathf.PI)
                {
                    rotation -= 2 * Mathf.PI;
                }
            }
            else
            {

                // calculate new rotation accounting for clock delta time
                rotation -= scaledRotationVelocity * TME_Manager.getDeltaTime(updateTime) * 60;
                if(rotation < 0)
                {
                    rotation += 2 * Mathf.PI;
                }
            }
        }

        // set rotation of gameobject
        transform.rotation = Quaternion.identity;
        transform.Rotate(0, -rotation * 180 / Mathf.PI, 0, Space.World);
    }



    //
    // wrapper for moving, accounts for acceleration. All velocity calculated <= unit vector, speed multiplied on later
    //
    public void move(Vector2 setTargetVelocity)
    {
        targetVelocity = setTargetVelocity;
    }



    //
    // causes character to jump
    //
    public void jump()
    {
        addJumpForce += 1;
    }



    //
    // Updates the motor by one tick
    //
    public void FixedUpdate()
    {
        // get Velocity
        Vector2 velocityXZ = getVelocity2D();
        float velocityY = getVelocity().y;

        // Update past location now that we don't need to get velocity
        pastLocation = transform.position;
        velocityY += gravity * TME_Manager.getDeltaTime(updateTime);

        targetVelocity *= TME_Manager.getDeltaTime(updateTime) * speed;
        
        if(addJumpForce > 0)
        {
            velocityY = addJumpForce * jumpForce;
            addJumpForce = 0;
        }

        if ((targetVelocity - velocityXZ).magnitude <= acceleration)
        {
            velocityXZ = targetVelocity;
        }
        else
        {
            // add acceleration as a vector with "acceleration" variable multiplied as scalar
            velocityXZ += (targetVelocity - velocityXZ).normalized * acceleration;
        }

        // update character's rotation
        if (targetVelocity.magnitude > 0)
        {
            setRotation(targetVelocity);
        }

        // apply translation to the object
        if (clampVelocityToDirection)
        {
            // only move in direction that character is facing
            physicalMove(UTL_Math.angleRadToUnitVec(rotation) * velocityXZ.magnitude * TME_Manager.getDeltaTime(updateTime) * 5 * speed);
        }
        else {
            // move normally
            Vector3 finalVelocity = new Vector3(velocityXZ.x, velocityY, velocityXZ.y);
            physicalMove(finalVelocity); //* TME_Manager.getDeltaTime(updateTime) * 5 * speed);
        }

        // update animation velocity based on physical distance moved
        if (sendAnimData)
        {
            // get the offset since last frame
            Vector3 delta = transform.position - pastLocation;

            // get velocity and animation scalar accounting for the scalar of the clock
            float velScalar = (delta.magnitude * 11);
            float animScalar = (0.75f + velScalar * .16f);

            // set the velocity and anim scalar varibles within the animator
            if (animator != null) {
                animator.SetFloat("velocity", velScalar);
                animator.SetFloat("velocityAnimScalar", animScalar);
            }
            // only send sound data if needed
            if (sendSoundData)
            {
                soundSystem.velocity = velScalar;
            }
        }

        // apply forces
        /*if(forceVel.magnitude > 0)
        {
            if(forceVel.magnitude <= forceAccel)
            {
                forceVel = Vector2.zero;
            }
            else
            {
                forceVel -= forceVel.normalized * forceAccel;
            }
            physicalMove(forceVel);
        }*/

        // reset target velocity
        targetVelocity = Vector2.zero;
    }



    //
    // Adds force to this motor
    //
    public void addForce(Vector2 force)
    {
        forceVel += force;
    }



    //
    // Moves towards a point
    //
    public void moveTowardsPoint(Vector2 targetPoint)
    {
        DBG_Debug.circle(targetPoint, .5f, Color.yellow);
        Vector2 myLocation = new Vector2(transform.position.x, transform.position.y);
        Vector2 moveVec = targetPoint - myLocation;
        moveVec = moveVec.normalized;
        move(moveVec);
    }



    //
    // moves motor along predefined path. Return true when reached end of path
    //
    public bool moveAlongPath(PTH_Path p)
    {

        // get the current position
        Vector2 current_position = transform.position;
        Vector2 nextPoint = p.getNextPoint();

        // runs if we have not reached the next point
        if (Vector2.Distance(current_position, nextPoint) > 0.3f)
        {
            moveTowardsPoint(nextPoint);
            return false;
        }
        // runs if we have reached next point
        else
        {

            // see if this is the end of the path
            if (p.atLastPoint())
            {
                return true;
            }

            // incriment to the next point
            p.incrimentPoint();
            nextPoint = p.getNextPoint();
            moveTowardsPoint(nextPoint);
            return false;
        }
    }



    //
    // moves object exactly x and y units
    //
    private void physicalMove(Vector3 vec)
    {
        float floorLevel = UTL_Dungeon.getTerrPosition(transform.position).y;
        gameObject.transform.Translate(vec /** inclineVel*/, Space.World);
        if(transform.position.y < floorLevel)
        {
            transform.position = new Vector3(transform.position.x, floorLevel, transform.position.z);
        }
    }



    //
    // getter for rotation field
    //
    public float getRotation()
    {
        return rotation;
    }


    //
    // returns the physical velocity for this frame
    //
    public float getVelocityMagnitude()
    {
        return (transform.position - pastLocation).magnitude / TME_Manager.getTimeScalar(updateTime);
    }


    //
    // Get 2D velocity (x/z plane)
    //
    public Vector2 getVelocity2D()
    {
        return UTL_Math.vec3ToVec2((transform.position - pastLocation) / TME_Manager.getTimeScalar(updateTime));
    }



    //
    // Get the velocity
    //
    public Vector3 getVelocity()
    {
        return (transform.position - pastLocation) / TME_Manager.getTimeScalar(updateTime);
    }
}
