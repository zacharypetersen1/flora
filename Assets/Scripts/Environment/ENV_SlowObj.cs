﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_SlowObj : MonoBehaviour {

    public float startGrowthTimer = 0f;
    float timer;
    public float distToGrow = 3;

    void Awake()
    {
        startGrowthTimer = Random.Range(.2f, 3f);
        timer = startGrowthTimer;
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(timer> 0)
        {
            timer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            transform.position = new Vector3(transform.position.x, transform.position.y + (distToGrow / startGrowthTimer) * TME_Manager.getDeltaTime(TME_Time.type.environment), transform.position.z);
        }
	}
}
