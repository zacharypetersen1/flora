﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;

namespace BBUnity.Conditions
{
    [Condition("Custom/FollowShouldMove")]
    [Help("checks if the monster should move based on whether they and the midpoint of all allies are near Twig")]
    public class FollowShouldMove : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkShouldFollow");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                bool ImNearTwig = body.allyManager.amINearTwig(gameObject.transform.position);
                bool MidpointNearTwig = body.allyManager.isMidpointNearTwig();
                if (!(ImNearTwig))
                {
                    //Debug.Log(gameObject.name + " is not near twig");
                    return true;
                }
                else if (!(MidpointNearTwig))
                {
                    //Debug.Log("ally midpoint " + monsterBrain.allyManager.getMidpoint() + " is not near twig" + monsterBrain.allyManager.transform.position);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw new UnityException("error in checkShouldFollow node");
            }
        }
    }
}