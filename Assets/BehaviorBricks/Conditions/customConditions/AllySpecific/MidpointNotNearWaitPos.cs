﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/MidpointNotNearWaitPos")]
    [Help("Checks if the midpoint of all allies is not within the AllyFollowRadius of their WaitPos")]
    public class MidpointNotNearWaitPos : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("notNearMidpointRes: " + body.isMidpointNearTwig());
                return !body.isMidpointNearWaitPos();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}