﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NotWithinFarFollowRadius")]
    [Help("Checks if the monster is notwithin FarFollowRadius of Twig")]
    public class NotWithinFarFollowRadius : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("self not near twig res: " + !body.amINearTwig(gameObject.transform.position));
                return Vector3.Distance(body.transform.position, body.Player.transform.position) > body.farFollowRadius;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}