﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{

    [Condition("Custom/Distance/finishedSearchLeft")]
    [Help("checks if the minion has rotated enough to the left")]
    public class finishedSearchLeft : GOCondition
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override bool Check()
        {
            const float sensitivity = 3;
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            //Debug.Log(body.finishedSearchLeft);
            if (body.finishedSearchLeft) return true;
            bool result = Mathf.Abs(Vector3.Distance(body.transform.position, body.searchLeft)) < sensitivity; //the one is important and magic, don't remove it!
            //Debug.Log("Pos: " + body.transform.position + ", searchLeft: " + body.searchLeft);
            //Debug.Log(Vector3.Angle(body.transform.forward, body.lookaroundLeft));
            if (result) body.finishedSearchLeft = true;
            return result;
        }
    }
}

