﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/EnemyAttack")]
    [Help("attack action for enemy monsters. attacks nearest ally, assumes it is within attackRange")]
    public class EnemyAttack : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                GameObject nearestAlly = monsterBody.getNearestAlly();
                monsterBody.attackTarget(nearestAlly);
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
