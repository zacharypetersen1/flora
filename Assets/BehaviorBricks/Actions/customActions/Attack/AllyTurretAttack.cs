﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Attack/AllyTurretAttack")]
    [Help("attack action for allied monsters. attacks nearest enemy, assumes it is within attackRange")]
    public class AllyTurretAttack : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("attack");

            try
            {
                ENT_TurretMinion monsterBody = gameObject.GetComponent<ENT_TurretMinion>();
                GameObject nearestEnemy = monsterBody.setCurrentTarget();
                if (nearestEnemy == null) return;
                else monsterBody.attackTarget(nearestEnemy);
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
