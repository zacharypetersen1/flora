﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToNearestVisibleEnemy")]
    [Help("Moves the monster towards the nearest enemy position")]
    public class moveToNearestVisibleEnemy : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestVisibleEnemy(gameObject.transform.position, body.sightRange);
                if (nearestEnemy != null)
                {
                    
                    Vector3 vec = gameObject.transform.position - body.Player.transform.position;
                    float magnitude = vec.magnitude;
                    Vector3 dirToTarget = vec / magnitude;
                    Vector3 finalPos = dirToTarget * (body.attackRange / 2);
                    finalPos = body.Player.transform.position + finalPos;
                    if (!body.isAtPosition(finalPos, 2))
                        body.goToPosition(finalPos);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}