﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/DoMove")]
    [Help("Tells the character to move along whatever path already existed for them")]
    public class DoMove : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("DoMove");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //body.shouldMove(true);
            }
            catch
            {
                throw new UnityException("DontMove behavior is broken and sad");
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
