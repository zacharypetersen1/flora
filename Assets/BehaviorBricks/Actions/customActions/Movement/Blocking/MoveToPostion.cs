﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/Blocking/MoveToPosition")]
    [Help("Moves the monster to a specified position")]
    public class CustomMoveToPosition : GOAction
    {
        [InParam("target")]
        [Help("Target position where the game object will be moved")]
        public Vector2 target;

        //private DBG_MonsterTest monster;
        //private float elapsedTime;
        ENT_Body monsterBody;
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                monsterBody = gameObject.GetComponent<ENT_Body>();
                monsterBody.goToPosition(target);
                //monster.ActionFunctionName();
            }
            catch
            {
                throw new UnityException("MoveToPosition is broken and sad");
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}