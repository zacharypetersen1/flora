﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Other/SwitchTreeAttack")]
    [Help("switches to the monster attack tree")]
    public class SwitchTreeAttack : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            //Debug.Log("ChangeGoal");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.CurrentTree = ENT_Body.BehaviorTree.AttackTree;
            }
            catch (Exception e)
            {
                throw e;
            }
            //monster = gameObject.GetComponent<DBG_MonsterTest>();
            //monster.attack();
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
