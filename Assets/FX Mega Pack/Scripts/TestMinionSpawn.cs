﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMinionSpawn : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UTL_Dungeon.spawnMinion(ENT_DungeonEntity.monsterTypes.treeMinion, 80, 30, 125, false);
        UTL_Dungeon.spawnMinion(ENT_DungeonEntity.monsterTypes.mushroomMinion, 90, 30, 125, false);
        UTL_Dungeon.spawnMinion(ENT_DungeonEntity.monsterTypes.treeMinion, 87, 30, 125, false);
    }
}
