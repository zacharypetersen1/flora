

float4 getWorldWindPos(float4 localPos, float4 worldPos, float time, float windScalar, float windFrequency) {
	
	float4 objWorld = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
	float theta = sin(time * windFrequency + objWorld.x + objWorld.y) * localPos.y * windScalar;
	float cosT = cos(theta);
	float sinT = sin(theta);

	/*float xOffset = sin(worldPos.x*0.3 + worldPos.y*0.3 - localPos.y*0.3 + time*1) * localPos.y*0.035 * windScalar;
	float yOffset = sin(worldPos.x*0.3 + worldPos.y*0.3 - localPos.y*0.3 + time*1+1.5) * localPos.y*0.035 * windScalar;
	*/

	float4x4 RotationMatrix = float4x4(	1,		0,		0,		0,
										0,		cosT,	-sinT,	0,
										0,		sinT,	cosT,	0,
										0,		0,		0,		1);
	
	float4 newLocPos = mul(RotationMatrix, localPos);
	return mul(unity_ObjectToWorld, newLocPos);
	
	/*worldPos.x += xOffset;
	worldPos.y += yOffset;
	return worldPos;*/
}