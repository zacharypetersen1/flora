from PIL import Image, ImageDraw
import random

imageSize = 512
steps = 20
strokesPerStep = 10
brushLifetime = 15
brushArr = []
colorToAnchor = {}

class BrushStroke():
	def __init__(self, sAnchorX, sAnchorY, sAnchorStep, sSize, sVelocity, sId):
		self.myId = sId
		self.color = (int(int(self.myId / 255) / 255), int(self.myId / 255) % 255, self.myId % 255, 255)
		self.anchorX = sAnchorX
		self.anchorY = sAnchorY
		colorToAnchor [self.color] = (self.anchorX, self.anchorY)
		self.anchorStep = sAnchorStep
		self.size = sSize
		self.velocity = sVelocity
		# Run "simulation" to calculate positions per step
		curX = self.anchorX
		curY = self.anchorY
		self.positions = {}
		simulationDone = False
		strokeActive = False
		strokeSteps = brushLifetime
		for i in range(0, steps * 2):
			j = i % steps
			if(not simulationDone):
				if(j == self.anchorStep):
					strokeActive = True
				if(strokeActive):
					self.positions[j] = (curX, curY)
					curX += self.velocity
					strokeSteps -= 1
				if(strokeSteps == 0):
					simulationDone = True



	def drawAtTime(self, step, drawImg):
		drawImg.ellipse((self.positions[step][0]-self.size, self.positions[step][1]-self.size, self.positions[step][0]+self.size, self.positions[step][1]+self.size), fill=self.color) 

def makeBrushStroke(strokeId):
	x = random.random() * imageSize
	y = random.random() * imageSize
	step = random.randint(0, steps-1)
	size = 5
	vel = 1
	return BrushStroke(x, y, step, size, vel, strokeId)

def drawStep(stepNum, drawIm):
	for brushStroke in brushArr:
		if(stepNum in brushStroke.positions):
			brushStroke.drawAtTime(stepNum, drawIm)

def drawImageAtStep(stepNum, baseIm):
	thisIm = baseIm.copy()
	thisDrawIm = ImageDraw.Draw(thisIm)
	for f in range(0, stepNum + 1):
		drawStep(f, thisDrawIm)
	thisIm.save("test" + str(stepNum).zfill(3) + ".png")

if __name__ == "__main__":
	
	# Initialize strokes
	for q in range(0, strokesPerStep * steps):
		brushArr.append(makeBrushStroke(q))

	# Initialize base image
	baseIm = Image.new("RGBA", (imageSize, imageSize), "white")
	baseDrawIm = ImageDraw.Draw(baseIm)
	for z in range(0, steps):
		drawStep(z, baseDrawIm)

	# Draw each step
	for p in range(0, steps):
		drawImageAtStep(p, baseIm)
