﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayVideo : MonoBehaviour {

    

	// Use this for initialization
	void Start () {
        MovieTexture mov = GetComponent<RawImage>().texture as MovieTexture;

        mov.loop = true;
        mov.anisoLevel = 50;
        mov.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
