﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionAnim : MonoBehaviour {

    Animator anim;
    private Vector3 pastPos;
    private float clampLower = 0.3f, clampUpper = 3;
    private float mapLower = 1, mapUpper = 2.5f;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        pastPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.T))
        {
            anim.SetTrigger("doMelee");
        }

        float velScalar = (pastPos - transform.position).magnitude * TME_Manager.getDeltaTime(TME_Time.type.game) * 400;
        velScalar = Mathf.Clamp(velScalar, 0, clampUpper);
        if(velScalar > clampLower)
        {
            velScalar = UTL_Math.mapToNewRange(velScalar, clampLower, clampUpper, mapLower, mapUpper);
        }
        else
        {
            velScalar = 0;
        }
        anim.SetFloat("velocity", velScalar);
        pastPos = transform.position;
	}


    void TestFunction()
    {
        print("hello minion");
    }
}
